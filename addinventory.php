<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                            <li class="page-item"><a class="page-link" href="inventory.php"><i class="bx bx-box"></i><small> Inventory</small></a></li>
                            <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-plus"></i><small> Add Inventory</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="d-flex flex-column justify-content-center align-items-center">
                <h2 class="page-title"><i class="bx bx-box"></i> Add Inventory </h2>
                <div class="container">
                    <div class="card">
                        <div class="card-body">
                            <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
                            <div class="form-group row">
                                <label for="item_name" class="col-sm-2 col-form-label">Item Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="item_name" id="item_name">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="description" class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="description" id="description">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="quantity" class="col-sm-2 col-form-label">Quantity</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="quantity" id="quantity">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="rating" class="col-sm-2 col-form-label">Rating</label>
                                <div class="col-sm-10">
                                    <div id="rating"></div>
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-2 col-form-label">Price</label>
                                <div class="col-sm-10">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">RM</span>
                                        <input type="text" class="form-control" autocomplete="off" name="price" id="price" onkeypress="return isNumberKey(this);">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="image" class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" autocomplete="off" name="image" id="image">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <center>
                                <div class="button-row" style="margin-right: 300px;">
                                    <div><a id="btnAdd" title="Add Inventory"></a></div>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $("#rating").rateYo({
            starWidth: "24px"
        });

        $('#btnAdd').click(function(){
            var formData = new FormData();
            formData.append('id',$('#id').val());
            formData.append('item_name',$('#item_name').val());
            formData.append('description',$('#description').val());
            formData.append('quantity',$('#quantity').val());
            formData.append('rating',$('#rating').rateYo("rating"));
            formData.append('price',$('#price').val());
            formData.append('image',$('#image').prop('files')[0]);
            
            $.ajax({
                type:'POST',
                url:'actioninventory.php?action=add',
                data:formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'inventory.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode
            console.log(charCode);
            if ((charCode > 47 && charCode < 58) || charCode == 46) {
                return true;
            }
            return false;
        }
    </script>
</body>
</html>