<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section id="hero" class="d-flex flex-column justify-content-center align-items-center">
                <div class="hero-container" data-aos="fade-in">
                    <h1><?php echo $name ?></h1>
                    <p>Welcome to <span class="typed" data-typed-items="Nurmawadah Hair Saloon"></span></p>
                    <span data-typed-text data-loop="true" data-type-speed="45" data-strings='["launch your product.","makeover your business.","prove your concept.","showcase your business."]'></span>
                </div>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
</body>
</html>