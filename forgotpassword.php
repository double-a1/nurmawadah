<?php
    include 'connect.php';

    function check_user($username, $conn){
        $row = mysqli_query($conn, "SELECT username FROM user WHERE username = '$username'");
        if(mysqli_num_rows($row)==1){
            return true;
        }else{
            return false;
        }
    }

    $msg = ''; $username = ''; $password = ''; $cpassword = '';
    if(isset($_POST['submit'])){
        $username = $_POST['username'];
        $password= $_POST['password'];
        $cpassword= $_POST['cpass'];
        
        if(empty($username)){
            $msg = "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
                            Toast.fire({
                                text: 'Please Enter Your Username!',
                                icon: 'error',
                                position: 'top-end',
                            })
                        </script>";
        }else if(empty($password)){
            $msg = "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
                            Toast.fire({
                                text: 'Please Enter Your New Password!',
                                icon: 'error',
                                position: 'top-end',
                            })
                        </script>";
        }
        else if(empty($cpassword)){
            $msg = "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
                            Toast.fire({
                                text: 'Please Enter Your Confirm Password!',
                                icon: 'error',
                                position: 'top-end',
                            })
                        </script>";
        }else if($password!=$cpassword){
            $msg = "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
                            Toast.fire({
                                text: 'Password does not match!',
                                icon: 'error',
                                position: 'top-end',
                            })
                        </script>";
        }else if(check_user($username,$conn)){   
            $username = $_POST['username'];
            $result = mysqli_query($conn, "SELECT username FROM user WHERE username ='$username'");
            $retrive= mysqli_fetch_array($result);
            $id = $retrive['username'];
            if($username == $id){
                $pass = md5($password);
                mysqli_query($conn, "UPDATE user SET password='$pass' WHERE username='$username'");
                $msg = "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
                                Toast.fire({
                                    text: 'Password updated successfully!',
                                    icon: 'success',
                                }).then(()=>{
                                    location.href = 'login.php';
                                })
                            </script>";
            }
        }else {
            $msg = "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
                            Toast.fire({
                                text: 'Username does not exist!',
                                icon: 'error',
                                position: 'top-end',
                            })
                        </script>";
        }
    }
?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
    <title>NURMAWADAH - FORGOT PASSWORD</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
	<link href="assets/vendor/fontawesome/css/all.min.css" rel="stylesheet">
	<link href="assets/css/index.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<div class="top"></div>
		<div class="bottom"></div>
		<div class="center">
			<h4>NURMAWADAH MUSLIMAH HAIR SALOON</h4>
			<h3>FORGOT PASSWORD</h3>\
            <script src="assets/vendor/sweetalert/main.min.js"></script>
            <script>
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })
            </script>
				<?php echo $msg; ?>
				<form name="myForm" action="forgotpassword.php" method="post">
				<input type="text" name="username" id="username" value="<?php echo $username; ?>" placeholder="Enter Username" autocomplete="off">
				<input type="password" id="password" name="password" value="<?php echo $password; ?>" placeholder="Enter New Password" autocomplete="off">
				<input type="password" id="cpass" name="cpass" value="<?php echo $cpassword; ?>" placeholder="Enter Confirm Password" autocomplete="off">
				<button name="submit">SUBMIT</button>
				<div id="signin">
				<a href="login.php">&nbsp;Sign In</a>
				</div>
			<form>
		</div>
	</div>
</body>
</html>