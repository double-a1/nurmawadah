<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href="assets/css/transaction.css" rel="stylesheet">
    <link href="assets/css/customerlist.css" rel="stylesheet">
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                            <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-group"></i><small> Customer List</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="d-flex flex-column justify-content-center align-items-center">
                <center>
                    <h2 class="page-title"><i class="bx bx-group"></i> Customer List </h2>
                    <div class="center">
                        <button class="fancy">
                            <span class="top-key"></span>
                            <a href="#" data-bs-toggle="modal" data-bs-target="#AddCustomer">Add Customer</a>
                            <span class="bottom-key-1"></span>
                            <span class="bottom-key-2"></span>
                        </button>
                    </div>
                </center>
                <div class="container mt-5 px-2">
                    <div class="table-responsive">
                        <table id="customerlist" class="table table-striped table-bordered">
                            <thead class="table-dark">
                                <tr>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">No</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Customer Name</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Contact No</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Total Visit</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <!-- Add Customer -->
                <div id="AddCustomer" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form enctype='multipart/form-data' method="POST" action="" class="well form-horizontal">
                                <div class="modal-header">
                                    <h5 class="modal-title">Add Customer</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i style="color:white" class="fas fa-times-circle"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="card">
                                        <div class="card-body">
                                            <input type="hidden" name="id" id="id">
                                            <div class="form-group row">
                                                <label for="customer_name" class="col-sm-3 col-form-label">Customer Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" autocomplete="off" name="customer_name" id="customer_name">
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="contact_no" class="col-sm-3 col-form-label">Contact No</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" autocomplete="off" name="contact_no" id="contact_no">
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <div class="button-row" style="margin-right: 400px;">
                                            <div class="modal-button-save"><a id="btnAdd" title="Add Customer"></a></div>
                                        </div>
                                    </center>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Add Customer -->

                <!-- Edit Customer -->
                <div id="EditCustomer" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form enctype='multipart/form-data' method="POST" action="" class="well form-horizontal">
                                <div class="modal-header">
                                    <h5 class="modal-title">Edit Customer</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i style="color:white" class="fas fa-times-circle"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="card">
                                        <div class="card-body">
                                            <input type="hidden" name="id" id="id">
                                            <div class="form-group row">
                                                <label for="customer_name" class="col-sm-3 col-form-label">Customer Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" autocomplete="off" name="customer_name" id="customer_name">
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="contact_no" class="col-sm-3 col-form-label">Contact No</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" autocomplete="off" name="contact_no" id="contact_no">
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <div class="button-row" style="margin-right: 250px;">
                                            <div class="modal-button-save"><a id="btnEdit" title="Edit Customer"></a></div>
                                        </div>
                                        <div class="button-row" style="margin-right: 250px;">
                                            <div class="modal-button-delete"><a id="btnDelete" title="Delete Customer"></a></div>
                                        </div>
                                    </center>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Edit Customer -->
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $('#btnAdd').click(function(){
            obj = {
                id:$('#AddCustomer #id').val(),
                customer_name:$('#AddCustomer #customer_name').val(),
                contact_no:$('#AddCustomer #contact_no').val()
            };
            
            $.ajax({
                type:'POST',
                url:'actioncustomerlist.php?action=add',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'customerlist.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        $('#btnEdit').click(function(){
            obj = {
                id:$('#EditCustomer #id').val(),
                customer_name:$('#EditCustomer #customer_name').val(),
                contact_no:$('#EditCustomer #contact_no').val()
            };
            
            $.ajax({
                type:'POST',
                url:'actioncustomerlist.php?action=edit',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'customerlist.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        $('#btnDelete').click(function(){
            obj = {
                id:$('#EditCustomer #id').val(),
            };
            
            $.ajax({
                type:'POST',
                url:'actioncustomerlist.php?action=delete',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'customerlist.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        $(document).on('click','.openCustomer',function(){
            var id = $(this).data('id');

            $.ajax({
                url: 'actioncustomerlist.php?id='+id+'&action=getCustomer',
                success: function(data){
                    $('#EditCustomer #id').val(data.id);
                    $('#EditCustomer #customer_name').val(data.customer_name);
                    $('#EditCustomer #contact_no').val(data.contact_no);
                }
            });
        });

        $('.close').click(function(){
            $('.modal').modal('hide');
        });
    </script>
</body>
</html>