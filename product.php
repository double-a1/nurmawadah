<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href="assets/css/product.css" rel="stylesheet">
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                            <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-box"></i><small> Product</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="flex-column justify-content-center align-items-center">
                <div id="modal-container">
                    <center>
                        <h1 class="page-title"><i class="bx bx-box"></i> Product </h1>
                    </center>
                    <div class="description">
                        <center>
                            <div class="center">
                                <button class="fancy">
                                    <span class="top-key"></span>
                                    <?php
                                        $sql = "SELECT COUNT(*) as total FROM cart WHERE status = 'progress' AND customer_name = '$name'";
                                        $statement = $conn->query($sql);
                                        $result = $statement->fetch_all(MYSQLI_ASSOC);
                                        $total_cart = $result[0]['total'];
                                        
                                        if ($result[0]) {
                                            echo '<a href="addproduct.php">Checkout<span class="badge">'.$total_cart.'</span></a>';
                                        } else {
                                            echo '<a href="addproduct.php">Checkout</a>';
                                        }
                                    ?>
                                    <span class="bottom-key-1"></span>
                                    <span class="bottom-key-2"></span>
                                </button>
                            </div>
                            <div class="container mt-2 px-5 py-5">
                                <div class="row">
                                    <?php
                                        $sql = "SELECT * FROM inventory";
                                        $statement = $conn->query($sql);
                                        $result = $statement->fetch_all(MYSQLI_ASSOC);
                                    
                                        foreach($result as $row){
                                            $sql_cart = "SELECT * FROM cart WHERE status = 'progress' AND customer_name = '$name'";
                                            $statement_cart = $conn->query($sql_cart);
                                            $cart = $statement_cart->fetch_all(MYSQLI_ASSOC);

                                            $array_cart = [];
                                            foreach($cart as $value){
                                                array_push($array_cart, $value['item_name']);
                                            }

                                            if(intval($row['quantity']) == 0 && in_array($row['item_name'], $array_cart)){
                                                $item_status = 'Out of stock';
                                                $action = '<ul class="product-links">
                                                                <li><a href="actionproduct.php?id='.$row["id"].'&price='.$row["price"].'&action=deleteCart"><i class="bx bx-minus"></i></a></li>
                                                            </ul>';
                                            } else if(intval($row['quantity']) == 0){
                                                $item_status = 'Out of stock';
                                                $action = '';
                                            } else{
                                                if(count($array_cart) != 0){
                                                    if (in_array($row['item_name'], $array_cart)) {
                                                        $item_status = '';
                                                        $action = '<ul class="product-links">
                                                                        <li><a href="actionproduct.php?id='.$row["id"].'&price='.$row["price"].'&action=deleteCart"><i class="bx bx-minus"></i></a></li>
                                                                        <li><a href="actionproduct.php?id='.$row["id"].'&item_name='.$row["item_name"].'&price='.$row["price"].'&action=add"><i class="bx bx-plus"></i></a></li>
                                                                    </ul>';
                                                    } else {
                                                        $item_status = '';
                                                        $action = '<ul class="product-links">
                                                                        <li><a href="actionproduct.php?id='.$row["id"].'&item_name='.$row["item_name"].'&price='.$row["price"].'&action=add"><i class="bx bx-plus"></i></a></li>
                                                                    </ul>';
                                                    }
                                                } else {
                                                    $item_status = '';
                                                    $action = '<ul class="product-links">
                                                                    <li><a href="actionproduct.php?id='.$row["id"].'&item_name='.$row["item_name"].'&price='.$row["price"].'&action=add"><i class="bx bx-plus"></i></a></li>
                                                                </ul>';
                                                }
                                            }
                                            echo    '<div class="col-md-3 col-sm-6" style="margin-bottom: 30px;">
                                                        <div class="product-grid">
                                                            <div class="product-image">
                                                                <a href="javascript:void(0)" class="image">
                                                                    <img class="pic-1" style="margin-top: 20px; width:125px !important; height:150px !important" src="assets/img/inventory/'.$row["image"].'">
                                                                    <img class="pic-2" style="margin-top: 20px; width:125px !important; height:150px !important; left: 20%;" src="assets/img/inventory/'.$row["image"].'">
                                                                </a>
                                                                '.$action.'
                                                            </div>
                                                            <div class="product-content" style="height:180px !important">
                                                                <div style="margin-bottom:10px" class="rating" data-rateyo-rating="'.$row["rating"].'"></div>
                                                                <h3 class="title"><a href="javascript:void(0)">'.$row["item_name"].'</a></h3>
                                                                <div class="price">RM '.$row["price"].'</div>
                                                                <div>Quantity: '.$row["quantity"].'</div>
                                                                <div style="color:red;font-weight:bold">'.$item_status.'</div>
                                                            </div>
                                                        </div>
                                                    </div>';
                                        }
                                    ?>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $(".rating").rateYo({
            starWidth: "20px",
            readOnly: true
        });
    </script>
</body>
</html>