<?php
    include 'connect.php';

    function check_user($username, $conn){
        $row = mysqli_query($conn, "SELECT username FROM user WHERE username = '$username'");
        if(mysqli_num_rows($row)==1){
            return true;
        }else{
            return false;
        }
    }

    $msg = ''; $username = ''; $password = ''; $cpassword = '';
    if(isset($_POST['submit'])){
        $username = $_POST['username'];
        $name = $_POST['name'];
        $contact_no = $_POST['contact_no'];
        $password = $_POST['password'];
        $cpassword = $_POST['cpass'];
        
        if(empty($username)){
            $msg = "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
                            Toast.fire({
                                text: 'Please Enter Your Username!',
                                icon: 'error',
                                position: 'top-end',
                            })
                        </script>";
        }else if(empty($password)){
            $msg = "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
                            Toast.fire({
                                text: 'Please Enter Your Password!',
                                icon: 'error',
                                position: 'top-end',
                            })
                        </script>";
        }
        else if(empty($cpassword)){
            $msg = "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
                            Toast.fire({
                                text: 'Please Enter Your Confirm Password!',
                                icon: 'error',
                                position: 'top-end',
                            })
                        </script>";
        }else if($password!=$cpassword){
            $msg = "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
                            Toast.fire({
                                text: 'Password does not match!',
                                icon: 'error',
                                position: 'top-end',
                            })
                        </script>";
        }else if(check_user($username,$conn)){   
            $msg = "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
                            Toast.fire({
                                text: 'This username already exist!',
                                icon: 'error',
                                position: 'top-end',
                            })
                        </script>";
        }else {
            $user_type = 'member';
			$password = md5($password);

			$query = "INSERT INTO user (
				username,
                name,
				contact_no, 
				user_type,
                password,
				updated_date)
			VALUES (
				'$username',
                '$name',
				'$contact_no',
				'$user_type',
                '$password',
				CURRENT_TIMESTAMP)";
            mysqli_query($conn, $query);
            $msg = "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
                            Toast.fire({
                                text: 'Account created successfully!',
                                icon: 'success',
                            }).then(()=>{
                                location.href = 'login.php';
                            })
                        </script>";
        }
    }
?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
    <title>NURMAWADAH - SIGN UP</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
	<link href="assets/vendor/fontawesome/css/all.min.css" rel="stylesheet">
	<link href="assets/css/index.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<div class="top"></div>
		<div class="bottom"></div>
		<div class="center" style="height: 550px !important; top: 40% !important;">
			<h4>NURMAWADAH MUSLIMAH HAIR SALOON</h4>
			<h3>MEMBER SIGN UP</h3>\
            <script src="assets/vendor/sweetalert/main.min.js"></script>
            <script>
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })
            </script>
				<?php echo $msg; ?>
				<form name="myForm" action="register.php" method="post">
				<input type="text" name="username" id="username" placeholder="Enter Username" autocomplete="off">
                <input type="text" id="name" name="name" placeholder="Enter Name" autocomplete="off">
                <input type="text" id="contact_no" name="contact_no" placeholder="Enter Phone No" autocomplete="off">
				<input type="password" id="password" name="password" placeholder="Enter Password" autocomplete="off">
				<input type="password" id="cpass" name="cpass" placeholder="Enter Confirm Password" autocomplete="off">
				<button name="submit">SUBMIT</button>
				<div id="signin">
				<a href="login.php">&nbsp;Sign In</a>
				</div>
			<form>
		</div>
	</div>
</body>
</html>