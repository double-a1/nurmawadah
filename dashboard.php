<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href="assets/css/transaction.css" rel="stylesheet">
    <link href="assets/css/customerlist.css" rel="stylesheet">
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
            <div class="container">
                <div class="container d-flex">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                    <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-chart"></i><small> Dashboard</small></a></li>
                </ul>
                </div>
            </div>
            </section>
            <section class="flex-column justify-content-center align-items-center">
                <div id="modal-container">
                    <h1 class="page-title"><i class="bx bx-chart"></i> Dashboard </h1>
                    <div style="width: 15%">
                        <select class="form-select" id="year">
                            <?php
                                echo '<option value="'.date("Y").'">'.date("Y").'</option>';
                                $year = 1;
                                for($i = 0; $i < 2; $i++){
                                    echo '<option value="'.date("Y",strtotime("-".$year." year")).'">'.date("Y",strtotime("-".$year." year")).'</option>';
                                    $year++;
                                }
                            ?>
                        </select>
                    </div>
                    <div class="chart">
                        <div id="customer"></div>
                    </div>
                    <div class="chart">
                        <div id="transaction"></div>
                    </div>
                    <div class="chart">
                        <div id="service"></div>
                    </div>
                </div>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $('#year').change(function(){
            $.ajax({
                type:'GET',
                url: 'actiondashboard.php?action=getCustomer&year='+$(this).val(),
                success:function(data){
                    var month = [
                        "January",
                        "February",
                        "Mac",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ];

                    var plot = [{
                        x: month,
                        y: data,
                        type: "bar",
                    }];

                    var layout = {
                        title: "Numbers of Customer by Month",
                    };

                    Plotly.newPlot("customer", plot, layout);
                },
            });

            $.ajax({
                type:'GET',
                url: 'actiondashboard.php?action=getTransaction&year='+$(this).val(),
                success:function(data){
                    var month = [
                        "January",
                        "February",
                        "Mac",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ];

                    var plot = [{
                        x: month,
                        y: data,
                        type: "lines",
                    }];

                    var layout = {
                        title: "Numbers of Sales by Month",
                    };

                    Plotly.newPlot("transaction", plot, layout);
                },
            });

            $.ajax({
                type:'GET',
                url: 'actiondashboard.php?action=getService&year='+$(this).val(),
                success:function(data){
                    var plot = [{
                        labels: data.label,
                        values: data.data,
                        hole: 0.5,
                        type: "pie",
                    }];

                    var layout = {
                        title: 'Favourite Service by Customer',
                        height: 500,
                        width: 700
                    };

                    Plotly.newPlot("service", plot, layout);
                },
            });
        });

        $.ajax({
            type:'GET',
            url: 'actiondashboard.php?action=getCustomer',
            success:function(data){
                var month = [
                    "January",
                    "February",
                    "Mac",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ];

                var plot = [{
                    x: month,
                    y: data,
                    type: "bar",
                }];

                var layout = {
                    title: "Numbers of Customer by Month",
                };

                Plotly.newPlot("customer", plot, layout);
            },
        });

        $.ajax({
            type:'GET',
            url: 'actiondashboard.php?action=getTransaction',
            success:function(data){
                var month = [
                    "January",
                    "February",
                    "Mac",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ];

                var plot = [{
                    x: month,
                    y: data,
                    type: "lines",
                }];

                var layout = {
                    title: "Numbers of Sales by Month",
                };

                Plotly.newPlot("transaction", plot, layout);
            },
        });

        $.ajax({
            type:'GET',
            url: 'actiondashboard.php?action=getService',
            success:function(data){
                var plot = [{
                    labels: data.label,
                    values: data.data,
                    hole: 0.5,
                    type: "pie",
                }];

                var layout = {
                    title: 'Favourite Service by Customer',
                    height: 500,
                    width: 700
                };

                Plotly.newPlot("service", plot, layout);
            },
        });
    </script>
</body>
</html>