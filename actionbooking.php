<?php
	header('Content-Type: application/json');

	session_start();
	include 'connect.php';

	$name = $_SESSION['name'];
	$user_type = $_SESSION['user_type'];

	$action = ( isset($_GET["action"]) && !empty($_GET["action"]) ) ? $_GET["action"] : 'form';

	switch($action) {
		case 'add' :
			$customer_name  = $_POST['customer_name'];
			$contact_no  = $_POST['contact_no'];
			$title = $_POST['title'];
			$service_type = $_POST['service_type'];
			$remarks  = $_POST['remarks'];
			$start_date  = $_POST['start_date'];
			$end_date  = $_POST['end_date'];
			$status  = $_POST['status'] ?? 'progress';

			$query = "INSERT INTO booking (
				customer_name, 
				contact_no, 
				title,
				service_type, 
				remarks, 
				start_date, 
				end_date, 
				status,
				updated_date)
			VALUES (
				'$customer_name', 
				'$contact_no', 
				'$title', 
				'$service_type', 
				'$remarks', 
				'$start_date', 
				'$end_date', 
				'$status',
				CURRENT_TIMESTAMP)";
			
			if(mysqli_query($conn, $query)){
				$query = "UPDATE user SET 
						booking_status = (SELECT COUNT(*) FROM booking WHERE status = 'pending'),
						updated_date = CURRENT_TIMESTAMP
						WHERE user_type IN ('admin', 'staff')";
				
				if(mysqli_query($conn, $query)){
					$msg = "Booking added successfully!";
					$status = true;
				}else{
					$msg = mysqli_error($conn);
					$status = false;
				}
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'edit':
			$id = $_POST['id'];
			$customer_name  = $_POST['customer_name'];
			$contact_no  = $_POST['contact_no'];
			$title = $_POST['title'];
			$service_type = $_POST['service_type'];
			$remarks  = $_POST['remarks'];
			$start_date  = $_POST['start_date'];
			$end_date  = $_POST['end_date'];
			$status  = $_POST['status'];

			if($status == 'complete'){
				$sql_check = "SELECT * FROM customer WHERE customer_name='$customer_name'";
				$result_check = mysqli_query($conn, $sql_check);
				$row_check = mysqli_fetch_array($result_check);

				if($row_check == null){
					$total_visit = 1;

					$query_cust = "INSERT INTO customer (
						customer_name, 
						contact_no, 
						total_visit,
						updated_date)
					VALUES (
						'$customer_name',
						'$contact_no',
						'$total_visit',
						CURRENT_TIMESTAMP)";
					
					mysqli_query($conn, $query_cust);
				}else{
					$total_visit = intval($row_check["total_visit"]) + 1;

					$query_edit = "UPDATE customer SET 
					total_visit = '$total_visit',
					updated_date = CURRENT_TIMESTAMP
					WHERE customer_name = '$customer_name'";
					
					mysqli_query($conn, $query_edit);
				}
				$service_list = explode(',',$service_type);
				$service = join("','",$service_list);
                $service = "'".$service."'";

				$sql = "SELECT * FROM service WHERE service_type IN ({$service})";
				$statement = $conn->query($sql);
				$result = $statement->fetch_all(MYSQLI_ASSOC);

				$price = '';
				$total_price = 0;
				foreach($result as $key => $row){
					if($key+1 < count($result)){
						$price .= 'RM '.$row["price"].' + ';
					}else{
						$price .= 'RM '.$row["price"];
					}
					$total_price = $total_price + floatval($row["price"]);
				}
				$paid = '0.00';
				$balance = '0.00';
				$status_transaction = 'progress';

				$query_add = "INSERT INTO transaction (
					customer_name,
					staff_name,
					service_type,
					price,
					total_price,
					paid,
					balance,
					status,
					date,
					updated_date)
				VALUES (
					'$customer_name', 
					'',
					'$service_type', 
					'$price',
					'$total_price',
					'$paid',
					'$balance',
					'$status_transaction',
					CURRENT_TIMESTAMP,
					CURRENT_TIMESTAMP)";
			
				mysqli_query($conn, $query_add);
			}

			$query = "UPDATE booking SET 
				customer_name = '$customer_name', 
				contact_no = '$contact_no', 
				title = '$title', 
				service_type = '$service_type', 
				remarks = '$remarks', 
				start_date = '$start_date', 
				end_date = '$end_date', 
				status = '$status',
				updated_date = CURRENT_TIMESTAMP
				WHERE id = '$id'";
			
			if(mysqli_query($conn, $query)){
				$msg = "Booking edited successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'delete':
			if(isset($_POST['id'])){
				$id = $_POST['id'];
				$query = "DELETE FROM booking WHERE id = '$id'";
				if(mysqli_query($conn, $query)){
					$msg = "Booking deleted successfully!";
					$status = true;
				}else{
					$msg = mysqli_error($conn);
					$status = false;
				}
			}
			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'checkDate':
			$date = $_POST["date"];

			$sql = "SELECT * FROM booking WHERE start_date='$date'";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_array($result);

			echo json_encode($row["contact_no"]);
		break;

		default:
			$sql = "SELECT * FROM service";
			$statement = $conn->query($sql);
			$result = $statement->fetch_all(MYSQLI_ASSOC);
			
			foreach($result as $row){
				$data[] = array(
					'service_type' => $row["service_type"],
					'color' 	   => $row["color"],
				);
			}

			$sql1 = "SELECT * FROM booking WHERE status != 'complete' AND status != 'reject'";
			
			$statement1 = $conn->query($sql1);
			$result1 = $statement1->fetch_all(MYSQLI_ASSOC);
			
			$data1 = [];
			foreach($result1 as $row){
				foreach($data as $service){
					$checklength = count(explode(',',$row["service_type"]));
					if($checklength == 1){
						if($service["service_type"] == $row["service_type"]){
							$color = $service["color"];
						}
					}else{
						$color = '#000000';
					}
				}

				if ($user_type == 'admin' ||
					$user_type == 'staff' ||
					($user_type == 'member' && $row["customer_name"] == $name)
				) {
					$editable = true;
				} else {
					$editable = false;
				}
				array_push($data1,[
					'id'            => $row["id"],
					'customer_name' => $row["customer_name"],
					'contact_no' 	=> $row["contact_no"],
					'title'   		=> $row["title"],
					'service_type'  => $row["service_type"],
					'remarks' 		=> $row["remarks"],
					'start'   		=> $row["start_date"],
					'status'     	=> $row["status"],
					'color'   		=> $color,
					'editable'		=> $editable,
				]);
			}
			echo json_encode($data1);
		break;
	}
 ?>