<?php
	header('Content-Type: application/json');

	session_start();
	include 'connect.php';

	$name = $_SESSION['name'];

	$action = ( isset($_GET["action"]) && !empty($_GET["action"]) ) ? $_GET["action"] : 'table';

	switch($action) {
		default:
			$sql = "SELECT * FROM booking WHERE customer_name = '$name'";
			$statement = $conn->query($sql);
			$result = $statement->fetch_all(MYSQLI_ASSOC);
			
			$data = [];
			foreach($result as $key => $row){
				$data[] = array(
                    'key'          => $key+1,
					'start_date'   => $row["start_date"],
					'title'        => $row["title"],
					'service_type' => $row["service_type"],
					'status'       => $row["status"],
				);
			}
			echo json_encode($data);
		break;
	}
 ?>