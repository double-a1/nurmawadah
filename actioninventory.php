<?php
	header('Content-Type: application/json');

	include 'connect.php';

	$action = $_GET["action"];

	switch($action) {
		case 'add' :
			$item_name = $_POST["item_name"];
			$description = $_POST["description"];
			$quantity = $_POST["quantity"];
			$rating = $_POST["rating"];
			$price = $_POST["price"];
            $image = $_FILES['image']['name'];

            if ( 0 < $_FILES['image']['error'] ) {
                $msg = $_FILES['image']['error'];
            } else {
				$extension = pathinfo($image, PATHINFO_EXTENSION);
				$file_name = rand().'.'.$extension;
                move_uploaded_file($_FILES['image']['tmp_name'], 'assets/img/inventory/' . $file_name);
            }

			$query = "INSERT INTO inventory (
				item_name, 
				description, 
				quantity,
				rating, 
				price,
                image,
				updated_date)
			VALUES (
				'$item_name', 
				'$description', 
				'$quantity', 
				'$rating', 
				'$price',
                '$file_name',
				CURRENT_TIMESTAMP)";
			
			if(mysqli_query($conn, $query)){
				$msg = "Inventory added successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'edit':
            $id = $_POST["id"];
			$item_name = $_POST["item_name"];
			$description = $_POST["description"];
			$quantity = $_POST["quantity"];
			$rating = $_POST["rating"];
			$price = $_POST["price"];
			
			$query = "UPDATE inventory SET 
				item_name = '$item_name', 
				description = '$description', 
				quantity = '$quantity', 
				rating = '$rating', 
				price = '$price',
				updated_date = CURRENT_TIMESTAMP
				WHERE id = '$id'";
			
			if(mysqli_query($conn, $query)){
				$msg = "Inventory edited successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

        case 'delete':
			$id = $_POST["id"];
			
			$query = "DELETE FROM inventory WHERE id = '$id'";
			if(mysqli_query($conn, $query)){
				$msg = "Inventory deleted successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}
			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

        case 'uploadimage' :
            $id = $_POST["id"];
            $image = $_FILES['edit_image']['name'];

            if ( 0 < $_FILES['edit_image']['error'] ) {
                $msg = $_FILES['edit_image']['error'];
            } else {
                unlink('assets/img/inventory/'.$_POST['old_image']);
				$extension = pathinfo($image, PATHINFO_EXTENSION);
				$file_name = rand().'.'.$extension;
                move_uploaded_file($_FILES['edit_image']['tmp_name'], 'assets/img/inventory/' . $file_name);
            }

			$query = "UPDATE inventory SET
                image = '$file_name',
                updated_date = CURRENT_TIMESTAMP
                WHERE id = '$id'";
			
			if(mysqli_query($conn, $query)){
				$msg = "File uploaded successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'plus_quantity':
            $id = $_GET["id"];
			$quantity = intval($_GET["quantity"]);

			$quantity = $quantity + 1;
			
			$query = "UPDATE inventory SET
				quantity = '$quantity',
				updated_date = CURRENT_TIMESTAMP
				WHERE id = '$id'";
			
			mysqli_query($conn, $query);

			header("location:inventory.php");
		break;

		case 'minus_quantity':
            $id = $_GET["id"];
			$quantity = intval($_GET["quantity"]);

			$quantity = $quantity - 1;
			
			$query = "UPDATE inventory SET
				quantity = '$quantity',
				updated_date = CURRENT_TIMESTAMP
				WHERE id = '$id'";
			
			mysqli_query($conn, $query);

			header("location:inventory.php");
		break;

		default:
		break;
	}
 ?>