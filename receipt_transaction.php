<?php
    session_start();
    include 'connect.php';
    include 'session.php';

    $id = $_GET["id"];

    $sql = "SELECT * FROM transaction WHERE id='$id'";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($result);

    $customer_name = $row["customer_name"];
    $staff_name = $row["staff_name"];
    $service_type = explode(',',$row["service_type"]);
    $price = explode(' + ',$row["price"]);
    $total_price = $row["total_price"];
    $paid = $row["paid"];
    $balance = $row["balance"];
    $date = $row["date"];
    $updated_date = $row["updated_date"];
?>
<style>
    @media screen {
        #printSection {
            display: none;
        }
    }
    * {
        margin: 0;
        padding: 0;
        font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        box-sizing: border-box;
        font-size: 14px;
    }

    img {
        max-width: 100%;
    }

    body {
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: none;
        width: 100% !important;
        height: 100%;
        line-height: 1.6;
    }

    table td {
        vertical-align: top;
    }

    body {
        background-color: #f6f6f6;
    }

    .body-wrap {
        background-color: #f6f6f6;
        width: 100%;
    }

    .container {
        display: block !important;
        max-width: 600px !important;
        margin: 0 auto !important;
        clear: both !important;
    }

    .content {
        max-width: 600px;
        margin: 0 auto;
        display: block;
        padding: 20px;
    }

    .main {
        background: #fff;
        border: 1px solid #e9e9e9;
        border-radius: 3px;
    }

    .content-wrap {
        padding: 20px;
    }

    .content-block {
        padding: 0 0 20px;
    }

    .header {
        width: 100%;
        margin-bottom: 20px;
    }

    .footer {
        width: 100%;
        clear: both;
        color: #999;
        padding: 20px;
    }
    .footer a {
        color: #999;
    }
    .footer p, .footer a, .footer unsubscribe, .footer td {
        font-size: 12px;
    }

    h1, h2, h3 {
        font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
        color: #000;
        margin: 40px 0 0;
        line-height: 1.2;
        font-weight: 400;
    }

    h1 {
        font-size: 32px;
        font-weight: 500;
    }

    h2 {
        font-size: 24px;
    }

    h3 {
        font-size: 18px;
    }

    h4 {
        font-size: 14px;
        font-weight: 600;
    }

    p, ul, ol {
        margin-bottom: 10px;
        font-weight: normal;
    }
    p li, ul li, ol li {
        margin-left: 5px;
        list-style-position: inside;
    }

    a {
        color: #1ab394;
        text-decoration: underline;
    }

    .btn-primary {
        text-decoration: none;
        color: #FFF;
        background-color: #1ab394;
        border: solid #1ab394;
        border-width: 5px 10px;
        line-height: 2;
        font-weight: bold;
        text-align: center;
        cursor: pointer;
        display: inline-block;
        border-radius: 5px;
        text-transform: capitalize;
    }

    .last {
        margin-bottom: 0;
    }

    .first {
        margin-top: 0;
    }

    .aligncenter {
        text-align: center;
    }

    .alignright {
        text-align: right;
    }

    .alignleft {
        text-align: left;
    }

    .clear {
        clear: both;
    }

    .alert {
        font-size: 16px;
        color: #fff;
        font-weight: 500;
        padding: 20px;
        text-align: center;
        border-radius: 3px 3px 0 0;
    }
    .alert a {
        color: #fff;
        text-decoration: none;
        font-weight: 500;
        font-size: 16px;
    }
    .alert.alert-warning {
        background: #f8ac59;
    }
    .alert.alert-bad {
        background: #ed5565;
    }
    .alert.alert-good {
        background: #1ab394;
    }

    .invoice {
        margin: 40px auto;
        text-align: left;
        width: 80%;
    }
    .invoice td {
        padding: 5px 0;
    }
    .invoice .invoice-items {
        width: 100%;
    }
    .invoice .invoice-items td {
        border-top: #eee 1px solid;
        padding:10px;
    }
    .invoice .invoice-items .open td {
        border-top: 2px solid #333;
    }
    .invoice .invoice-items .close td {
        border-bottom: 2px solid #333;
        font-weight: 700;
    }

    @media only screen and (max-width: 640px) {
        h1, h2, h3, h4 {
            font-weight: 600 !important;
            margin: 20px 0 5px !important;
        }

        h1 {
            font-size: 22px !important;
        }

        h2 {
            font-size: 18px !important;
        }

        h3 {
            font-size: 16px !important;
        }

        .container {
            width: 100% !important;
        }

        .content, .content-wrap {
            padding: 10px !important;
        }

        .invoice {
            width: 100% !important;
        }
    }

    .btn{
        font-size:16px;
        background-color:white;
        background-border:black;
        padding:5px 15px;
    }
    .btn:hover{
        color:white;
        background-color:black;
        background-border:black;
    }
</style>
<div id="printReceipt">
    <table class="body-wrap">
        <tbody>
            <tr>
                <td></td>
                <td class="container" width="600">
                    <div class="content">
                        <table class="main" width="100%" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td class="content-wrap aligncenter">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td class="content-block" style="text-align:center">
                                                        <h2>NURMAWADAH MUSLIMAH HAIR SALOON</h2>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="content-block">
                                                        <table class="invoice">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Invoice #<?php echo $id ?><br>Staff Name: <b><?php echo $staff_name ?></b><br>Customer Name: <b><?php echo $customer_name ?></b><br>Date: <b><?php echo $updated_date ?></b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table class="invoice-items" cellpadding="0" cellspacing="0">
                                                                            <tbody>
                                                                                <?php 
                                                                                    for($i = 0; $i<count($service_type); $i++){
                                                                                        echo '  <tr>
                                                                                                    <td>'.$service_type[$i].'</td>
                                                                                                    <td class="alignright">'.$price[$i].'</td>
                                                                                                </tr>';
                                                                                    }
                                                                                ?>
                                                                                <tr class="open">
                                                                                    <td class="alignright" width="70%">Total</td>
                                                                                    <td class="alignright">RM <?php echo number_format(floatval($total_price), 2) ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="alignright" width="70%">Paid</td>
                                                                                    <td class="alignright">RM <?php echo number_format(floatval($paid), 2) ?></td>
                                                                                </tr>
                                                                                <tr class="close">
                                                                                    <td class="alignright" width="70%">Balance</td>
                                                                                    <td class="alignright">RM <?php echo number_format(floatval($balance), 2) ?></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align:center">
                                                                        <button type="button" id="printBtn" class="btn">Print</button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="content-block" style="text-align:center">
                                                        B3. Plaza Parit Buntar, Jalan Taiping, 34200 Parit Buntar, Perak
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script>
    $("#printBtn").click(function(){
        printElement(document.getElementById("printReceipt"));  
        window.print();
        $('#printSection').empty();
        $('#printBtn').show();
    });

    function printElement(elem) {
        $('#printBtn').hide();
        var domClone = elem.cloneNode(true);
        var $printSection = document.getElementById("printSection");
        
        if (!$printSection) {
            var $printSection = document.createElement("div");
            $printSection.id = "printSection";
            // document.body.appendChild($printSection);
        }
        $printSection.innerHTML = "";
        $printSection.appendChild(domClone);
    }
</script>