<?php
	header('Content-Type: application/json');

	include 'connect.php';

	$action = ( isset($_GET["action"]) && !empty($_GET["action"]) ) ? $_GET["action"] : 'table';

	switch($action) {
		case 'edit':
			$id = $_GET['id'];
			$customer_name = $_GET['customer_name'];
			$status = $_GET['status'];
			$status_message = $status == 'progress' ? 'accept' : 'reject';
			$title = 'Booking - ' . $customer_name . ' [' . strtoupper($status) . ']';

			$query = "UPDATE booking SET
					title = '$title',
					status = '$status',
					updated_date = CURRENT_TIMESTAMP
					WHERE id = '$id'";
			
			if(mysqli_query($conn, $query)){
				$name = strtolower($customer_name);
				$query = "UPDATE user SET 
							booking_status = (SELECT COUNT(*) FROM booking WHERE status IN ('progress', 'reject') ),
							updated_date = CURRENT_TIMESTAMP
							WHERE username = '$name'";
				
				if(mysqli_query($conn, $query)){
					$msg = "Booking " . $status_message . " successfully!";
					$status = true;
				}else{
					$msg = mysqli_error($conn);
					$status = false;
				}				
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		default:
			$sql = "SELECT * FROM booking ORDER BY start_date DESC";
			$statement = $conn->query($sql);
			$result = $statement->fetch_all(MYSQLI_ASSOC);
			
			$data = [];
			foreach($result as $key => $row){
				$data[] = array(
                    'key'           => $key+1,
					'id'	        => $row["id"],
					'start_date'    => $row["start_date"],
					'title'         => $row["title"],
					'service_type'  => $row["service_type"],
					'status'        => $row["status"],
					'customer_name' => $row["customer_name"],
				);
			}
			echo json_encode($data);
		break;
	}
 ?>