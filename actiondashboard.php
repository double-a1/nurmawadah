<?php
	header('Content-Type: application/json');

	include 'connect.php';

	$action = ( isset($_GET["action"]) && !empty($_GET["action"]) ) ? $_GET["action"] : 'default';
	$year = ( isset($_GET["year"]) && !empty($_GET["year"]) ) ? $_GET["year"] : date("Y");

	switch($action) {
		case 'getCustomer' :
			$sql = "SELECT EXTRACT(MONTH from start_date) AS month, COUNT(EXTRACT(MONTH from start_date)) AS total FROM booking WHERE status = 'complete' AND YEAR(start_date) = '$year' GROUP BY EXTRACT(MONTH from start_date) ORDER BY month";
			$statement = $conn->query($sql);
			$result = $statement->fetch_all(MYSQLI_ASSOC);
			
			$data = [];
			for ($i = 0; $i < 12; $i++){
				array_push($data, 0);
			}

			foreach ($result as $value) {
				$data[intval($value['month'])-1] = $value["total"] ?? 0;
			}

			echo json_encode($data);
		break;

		case 'getTransaction' :
			$sql = "SELECT EXTRACT(MONTH from date) AS month, COUNT(EXTRACT(MONTH from date)) AS total FROM transaction WHERE status = 'complete' AND YEAR(date) = '$year' GROUP BY EXTRACT(MONTH from date) ORDER BY month";
			$statement = $conn->query($sql);
			$result = $statement->fetch_all(MYSQLI_ASSOC);
			
			$data = [];
			for ($i = 0; $i < 12; $i++){
				array_push($data, 0);
			}

			foreach ($result as $value) {
				$data[intval($value['month'])-1] = $value["total"] ?? 0;
			}

			echo json_encode($data);
		break;

		case 'getService' :
			$sql = "SELECT service_type FROM service";
			$statement = $conn->query($sql);
			$result = $statement->fetch_all(MYSQLI_ASSOC);

			$sql1 = "SELECT service_type FROM transaction WHERE status = 'complete' AND YEAR(date) = '$year'";
			$statement1 = $conn->query($sql1);
			$result1 = $statement1->fetch_all(MYSQLI_ASSOC);
			
			$type = [];
			foreach ($result as $value) {
				array_push($type, $value['service_type']);
			}

			$data = [];
			foreach ($type as $value) {
				$total = 0;
				foreach ($result1 as $service) {
					if (str_contains($service['service_type'], $value)) {
						$total++;
					}
				}
				array_push($data, $total);
			}

			echo json_encode([ "data" => $data, "label" => $type]);
		break;

		default:
			
		break;
	}
 ?>