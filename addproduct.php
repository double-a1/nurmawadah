<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                            <li class="page-item"><a class="page-link" href="product.php"><i class="bx bx-box"></i><small> Product</small></a></li>
                            <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-plus"></i><small> Checkout</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="flex-column justify-content-center align-items-center">
                <div id="form-container">
                    <h1 class="page-title"><i class="bx bx-box"></i> Checkout </h1>
                    <div class="description">
                        <div class="container h-100">
                            <div class="row d-flex justify-content-center align-items-center h-100">
                                <div class="col">
                                    <p><span class="h2">Shopping Cart </span></p>
                                    <div class="card mb-4">
                                        <?php
                                            $sql = "SELECT * FROM cart WHERE customer_name='$name' AND status = 'progress'";
                                            $statement = $conn->query($sql);
                                            $result = $statement->fetch_all(MYSQLI_ASSOC);
                                        
                                            $total_price = 0;
                                            $array_item = [];
                                            foreach($result as $row){
                                                $query = "SELECT image FROM inventory WHERE id=".$row['inventory_id']."";
                                                $fetch = $conn->query($query);
                                                $image = $fetch->fetch_all(MYSQLI_ASSOC);

                                                echo    '<div class="card-body p-4">
                                                            <div class="row align-items-center">
                                                                <div class="col-md-2">
                                                                    <img src="assets/img/inventory/'.$image[0]["image"].'"
                                                                    class="img-fluid" alt="'.$row['item_name'].'">
                                                                </div>
                                                                <div class="col-md-2 d-flex justify-content-center">
                                                                    <div>
                                                                        <p class="small text-muted mb-4 pb-2">Name</p>
                                                                        <p class="lead fw-normal mb-0">'.$row['item_name'].'</p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 d-flex justify-content-center">
                                                                    <div>
                                                                        <p class="small text-muted mb-4 pb-2">Quantity</p>
                                                                        <p class="lead fw-normal mb-0">'.$row['quantity'].'</p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 d-flex justify-content-center">
                                                                    <div>
                                                                        <p class="small text-muted mb-4 pb-2">Price</p>
                                                                        <p class="lead fw-normal mb-0">RM '.number_format($row['price'], 2).'</p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 d-flex justify-content-center">
                                                                    <div>
                                                                        <p class="small text-muted mb-4 pb-2">Total</p>
                                                                        <p class="lead fw-normal mb-0">RM '.number_format($row['total_price'], 2).'</p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 d-flex justify-content-center">
                                                                    <div>
                                                                        <p class="small text-muted mb-4 pb-2">Action</p>
                                                                        <p class="lead fw-normal mb-0"><a href="actionproduct.php?id='.$row["inventory_id"].'&price='.$row["price"].'&action=deleteCheckout"><i class="bx bx-trash"></i></a></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>';
                                                array_push($array_item, $row['inventory_id']);
                                                $total_price = $total_price + floatval($row['total_price']);
                                            }
                                        ?>
                                    </div>
                                    <div class="card mb-5">
                                        <div class="card-body p-4">
                                            <div class="float-end">
                                                <p class="mb-0 me-5 d-flex align-items-center">
                                                    <span class="small text-muted me-2">Order total:</span> <span
                                                    class="lead fw-normal">RM <?php echo number_format($total_price, 2); ?></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <a href="product.php"><button type="button" class="btn btn-light">Continue shopping</button></a>
                                        <div class="checkout" style="margin-right: 150px;">
                                            <div><a id="pay" title="Checkout & Pay"></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $("#rating").rateYo({
            starWidth: "24px"
        });

        $('#pay').click(function(){
            <?php 
                $_SESSION['total_price'] = $total_price;
                $_SESSION['item_id'] = implode(',', $array_item);

                if (count($result) == 0) {
                    echo "swal.fire({
                        title: 'Info!',
                        text: 'No item in the cart.',
                        icon: 'info',
                        customClass: 'swal-wide'
                      })";
                } else {
            ?>
                location.href = 'payment.php';
            <?php
                }
            ?>
        });
    </script>
</body>
</html>