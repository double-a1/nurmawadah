<?php
    session_start();
    include 'connect.php';
    include 'session.php';

    $id = $_GET["id"];

    $sql = "SELECT * FROM inventory WHERE id='$id'";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($result);

    $item_name = $row["item_name"];
    $description = $row["description"];
    $quantity = $row["quantity"];
    $rating = $row["rating"];
    $price = $row["price"];
    $image = $row["image"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                            <li class="page-item"><a class="page-link" href="inventory.php"><i class="bx bx-box"></i><small> Inventory</small></a></li>
                            <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-pencil"></i><small> Edit Inventory</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="d-flex flex-column justify-content-center align-items-center">
                <h2 class="page-title"><i class="bx bx-box"></i> Edit Inventory </h2>
                <div class="container">
                    <div class="card">
                        <div class="card-body">
                            <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
                            <div class="form-group row">
                                <label for="item_name" class="col-sm-2 col-form-label">Item Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="item_name" id="item_name" value="<?php echo $item_name ?>">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="description" class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="description" id="description" value="<?php echo $description ?>">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="quantity" class="col-sm-2 col-form-label">Quantity</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="quantity" id="quantity" value="<?php echo $quantity ?>">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="rating" class="col-sm-2 col-form-label">Rating</label>
                                <div class="col-sm-10">
                                    <div id="rating" data-rateyo-rating="<?php echo $rating ?>"></div>
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-2 col-form-label">Price</label>
                                <div class="col-sm-10">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">RM</span>
                                        <input type="text" class="form-control" autocomplete="off" name="price" id="price" value="<?php echo $price ?>" onkeypress="return isNumberKey(this);">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <?php if($image != null){ ?>
                                <div class="form-group row">
                                    <label for="image" class="col-sm-2 col-form-label">Image</label>
                                    <div class="col-sm-10">
                                        <div class="col-md-3" style="text-align:center;border: 2px solid">
                                            <img id="image_preview" style="margin-top: 20px; width:125px !important; height:150px !important" src="assets/img/inventory/<?php echo $image ?>">
                                            <div class="btn-group" style="display:flex;" role="group">
                                                <button type="button" style="border-top:2px solid;border-radius: 0;" class="btn btn-outline-dark edit-image"><i class="bx bx-pencil"></i></button>
                                                <button type="button" style="border-top:2px solid;border-radius: 0; display:none" class="btn btn-outline-dark upload-image"><i class="bx bx-upload"></i></button>
                                                <input type="file" style="display:none" class="form-control" onchange="readURL(this)" name="edit_image" id="edit_image">
                                                <input type="hidden" name="old_image" id="old_image" value="<?php echo $image ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php }else{ ?>
                                <div class="form-group row">
                                    <label for="image" class="col-sm-2 col-form-label">Image</label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" autocomplete="off" name="image" id="image">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="card-footer">
                            <center>
                                <div class="button-row" style="margin-right: 250px;">
                                    <div class="modal-button-save"><a id="btnEdit" title="Edit Inventory"></a></div>
                                </div>
                                <div class="button-row" style="margin-right: 250px;">
                                    <div class="modal-button-delete"><a id="btnDelete" title="Delete Inventory"></a></div>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $("#rating").rateYo({
            starWidth: "24px"
        });

        $('.edit-image').click(function(){
            $('#edit_image').trigger('click');
        });

        $('.upload-image').click(function(){
            var id = $('#id').val();

            var formData = new FormData();
            formData.append('id',$('#id').val());
            formData.append('old_image',$('#old_image').val());
            formData.append('edit_image',$('#edit_image').prop('files')[0]);

            $.ajax({
                type:'POST',
                url:'actioninventory.php?action=uploadimage',
                data:formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'editinventory.php?id='+id+''
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                $('.edit-image').hide();
                $('.upload-image').show();
            }
        }
        
        $('#btnEdit').click(function(){
            obj = {
                id:$('#id').val(),
                item_name:$('#item_name').val(),
                description:$('#description').val(),
                quantity:$('#quantity').val(),
                rating:$('#rating').rateYo("rating"),
                price:$('#price').val()
            };
            
            $.ajax({
                type:'POST',
                url:'actioninventory.php?action=edit',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'inventory.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        $('#btnDelete').click(function(){
            obj = {
                id:$('#id').val(),
            };
            
            $.ajax({
                type:'POST',
                url:'actioninventory.php?action=delete',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'inventory.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode
            console.log(charCode);
            if ((charCode > 47 && charCode < 58) || charCode == 46) {
                return true;
            }
            return false;
        }
    </script>
</body>
</html>