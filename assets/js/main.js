(function() {
  "use strict";

  $(window).on('load',function(){
    $("#loading").fadeOut('slow');
    $("#body").fadeIn('slow');
    var path = window.location.href;
    var list = path.split('/');
    var title = list[list.length - 1];
    if (title == 'booking.php' || title == 'booking_member.php') {
      var calendarEl = document.getElementById('calendar');
      var calendar = new FullCalendar.Calendar(calendarEl,{
        selectable: true,
        longPressDelay: 500,
        select: function(info) {
          info.jsEvent.preventDefault();
          $('#AddBooking #date').val(info.startStr);
          $('#AddBooking').modal('show');
        },
        eventDidMount: function(info) {
          $(info.el).attr('data-tooltip', 'tooltip');
          $(info.el).tooltip({
              title: info.event.title,
              placement: 'top',
              trigger: 'hover',
              container: 'body'
          });
        },
        events:'actionbooking.php',
        eventClick:function(info) {
          info.jsEvent.preventDefault();
          if(info.event.startEditable){
            $('#EditBooking input:checkbox').prop('checked',false);
            $('#EditBooking input:radio').prop('checked',false);
            var start = info.event.start;
            var split_date = start.toJSON();
            var start_date = split_date.split('T');
            var split_time = info.event.startStr.split('T');
            var start_time = split_time[1].split('+');
            var service_type =info.event.extendedProps.service_type.split(',');
            $('#EditBooking #id').val(info.event.id);
            $('#EditBooking #date').val(start_date[0]);
            $('#EditBooking #time').val(start_time[0]);
            $('#EditBooking #edit_customer_name').val(info.event.extendedProps.customer_name);
            for(var i = 0; i<service_type.length; i++){
              var id_key = service_type[i].replace(/ /g,"");
              $('#EditBooking #'+id_key+'').prop('checked',true);    
            }
            $('#EditBooking #contact_no').val(info.event.extendedProps.contact_no);
            $('#EditBooking #remarks').val(info.event.extendedProps.remarks);
            if(info.event.extendedProps.status == 'progress'){
              $('#EditBooking #progress').prop('checked',true);
              $('#statusDiv').show();
            }else if(info.event.extendedProps.status == 'complete'){
              $('#EditBooking #complete').prop('checked',true);
              $('#statusDiv').show();
            }else{
              $('#statusDiv').hide();
            }
            $('#EditBooking').modal('show');
          }
        }
      });
      calendar.render();
    }else if(title == 'transaction.php'){
      $('#transaction').DataTable({
        ajax: {
          url: 'actiontransaction.php',
          dataSrc: ''
        },
        ordering:false,
        columns: [
          {data: 'key'},
          {data: 'customer_name'},
          {
            data: 'staff_name',
            render: function(data,type,full){
                if(data == ''){
                    var html = '<div style="width:150px"><img width="25%" src="assets/img/avatar.png" class="img-fluid rounded-circle"><span> Not Assign</span></div>';
                }else{
                    if(full.image != null){
                        var html = '<div style="width:150px"><img width="25%" src="assets/img/profile/'+full.image+'" class="img-fluid rounded-circle"><span> '+data+'</span></div>';
                    }else{
                        var html = '<div style="width:150px"><img width="25%" src="assets/img/avatar.png" class="img-fluid rounded-circle"><span> '+data+'</span></div>';
                    }
                }
                return html;
            }    
          },
          {
            data: 'service_type',
            render: function(data,type,full){
                var service = data.split(',');
                var service_list = '';
                for(var i = 0; i<service.length; i++){
                    service_list += '<span style="margin-top:5px;padding:10px" class="badge rounded-pill bg-dark">'+service[i].toUpperCase()+'</span>';
                }
                var html = `<div style="display:grid;">
                                `+service_list+`
                            </div>`;
                return html;
            }    
          },
          {data: 'price'},
          {
            data: 'total_price',
            render: function(data,type,full){
                const formatter = new Intl.NumberFormat('ms-MY', {
                    style: 'currency',
                    currency: 'MYR',
                    minimumFractionDigits: 2
                });

                var total_price = parseFloat(data);
                var html = '<p>'+formatter.format(total_price)+'</p>';
                return html;
            }
          },
          {data: 'date'},
          {
            data: 'id',
            render: function(data,type,full){
                if(full.status == 'progress'){
                    var html = `<div style="display:grid;">
                                    <a class="btn btn-outline-dark openTransaction" data-id="`+data+`" data-bs-toggle="modal" data-bs-target="#EditTransaction"><i class="bx bx-calculator"></i></a>
                                </div>`;
                }else{
                    var html = `<div style="display:grid;">
                                    <a class="btn btn-outline-dark" href="receipt_transaction.php?id=`+full.id+`" target="_blank"><i class="bx bx-printer"></i></a>
                                </div>`;
                }
                return html;
            }
          },
        ]
      });
    }else if(title == 'customerlist.php'){
      $('#customerlist').DataTable({
        ajax: {
          url: 'actioncustomerlist.php',
          dataSrc: ''
        },
        ordering:false,
        columns: [
          {data: 'key'},
          {data: 'customer_name'},
          {data: 'contact_no'},
          {
            data: 'total_visit',
            render: function(data,type,full){
                var html = `<div style="display:grid;">
                                <i class="bx bx-walk badge bg-dark" style="font-size:20px;margin-top:5px;padding:10px"><span> `+data+`</span></i>
                            </div>`;
                return html;
            }
          },
          {
            data: 'id',
            render: function(data,type,full){
                var html = `<div style="display:grid;">
                                <a class="btn btn-outline-dark openCustomer" data-id="`+data+`" data-bs-toggle="modal" data-bs-target="#EditCustomer"><i class="bx bx-pencil"></i></a>
                            </div>`;
                return html;
            }
          },
        ]
      });
    }else if(title == 'stafflist.php'){
      $('#stafflist').DataTable({
        ajax: {
          url: 'actionstafflist.php',
          dataSrc: ''
        },
        ordering:false,
        columns: [
          {data: 'key'},
          {data: 'username'},
          {data: 'name'},
          {data: 'contact_no'},
          {
            data: 'user_type',
            render: function(data,type,full){
              var html = `<div style="display:grid;">
                              <span class="badge bg-dark" style="font-size:14px;margin-top:5px;padding:10px">`+data+`</span>
                          </div>`;
              return html;
            }
          },
          {
            data: 'id',
            render: function(data,type,full){
                var html = `<div style="display:grid;">
                                <a class="btn btn-outline-dark openStaff" data-id="`+data+`" data-bs-toggle="modal" data-bs-target="#EditStaff"><i class="bx bx-pencil"></i></a>
                            </div>`;
                return html;
            }
          },
        ] 
      });
    }else if(title == 'attendance_admin.php'){
      $('#attendance').DataTable({
        ajax: {
            url: 'actionattendance.php',
            dataSrc: ''
        },
        ordering:false,
        columns: [
          {data: 'key'},
          {data: 'username'},
          {data: 'checkin'},
          {data: 'checkout'}
        ]
      });
    }else if(title == 'history.php'){
      $('#historylist').DataTable({
        ajax: {
            url: 'actionhistory.php',
            dataSrc: ''
        },
        ordering:false,
        columns: [
          {data: 'key'},
          {data: 'start_date'},
          {data: 'title'},
          {data: 'service_type'},
          {
            data: 'status',
            render: function(data,type,full){
              var color = '';
              if (data == 'pending') {
                color = 'bg-dark';
              } else if (data == 'progress') {
                color = 'bg-warning';
              } else if (data == 'complete') {
                color = 'bg-success';
              } else if (data == 'reject') {
                color = 'bg-danger';
              }

              var html = `<div style="display:grid;">
                              <span class="badge `+color+`" style="font-size:14px;margin-top:5px;padding:10px">`+data+`</span>
                          </div>`;
              return html;
            }
          },
        ]
      });
    }else if(title == 'booking_detail.php'){
      $('#bookinglist').DataTable({
        ajax: {
            url: 'actionbookingdetail.php',
            dataSrc: ''
        },
        ordering:false,
        columns: [
          {data: 'key'},
          {data: 'start_date'},
          {data: 'title'},
          {data: 'service_type'},
          {
            data: 'status',
            render: function(data,type,full){
              var color = '';
              if (data == 'pending') {
                color = 'bg-dark';
              } else if (data == 'progress') {
                color = 'bg-warning';
              } else if (data == 'complete') {
                color = 'bg-success';
              } else if (data == 'reject') {
                color = 'bg-danger';
              }

              var html = `<div style="display:grid;">
                              <span class="badge `+color+`" style="font-size:14px;margin-top:5px;padding:10px">`+data+`</span>
                          </div>`;
              return html;
            }
          },
          {
            data: 'id',
            render: function(data,type,full){
              if (full.status == 'pending') {
                var html = `<div style="display:block;text-align:center">
                              <a onclick="actionBook(`+data+`,'progress','edit',\'`+full.customer_name+`\')" class="btn btn-success"><i class="bx bx-check"></i></a>
                              <a onclick="actionBook(`+data+`,'reject','edit',\'`+full.customer_name+`\')"  class="btn btn-danger rejectBook"><i class="bx bx-x"></i></a>
                            </div>`;
                return html;
              }else{
                var html = `<div style="display:block;text-align:center">
                              -
                            </div>`;
                return html;
              }
            }
          },
        ]
      });
    }else if(title == 'feedbacklist.php'){
      $('#feedbacklist').DataTable({
        ajax: {
            url: 'actionfeedback.php',
            dataSrc: ''
        },
        ordering:false,
        columns: [
          {data: 'key'},
          {data: 'name'},
          {data: 'question'},
          {
            data: 'status',
            render: function(data,type,full){
              var color = '';
              if (data == 'progress') {
                color = 'bg-warning';
              } else if (data == 'complete') {
                color = 'bg-success';
              }

              var html = `<div style="display:grid;">
                              <span class="badge `+color+`" style="font-size:14px;margin-top:5px;padding:10px">`+data+`</span>
                          </div>`;
              return html;
            }
          },
          {
            data: 'id',
            render: function(data,type,full){
                var html = `<div style="display:grid;">
                              <a class="btn btn-outline-dark openFeedback" data-id="`+data+`" data-bs-toggle="modal" data-bs-target="#EditFeedback"><i class="bx bx-pencil"></i></a>
                            </div>`;
                return html;
            }
          },
        ]
      });
    }
  });

  /**
   * Modal function
   */
  $('.modal').on('show', function(e) {
    var modal = $(this);
    modal.css('margin-top', (modal.outerHeight() / 2) * -1)
          .css('margin-left', (modal.outerWidth() / 2) * -1);
    return this;
  });

  /**
   * Active Navbar function
   */
  var path = window.location.href;
  $("#navbar ul a.nav-link").each(function() {
    if (this.href == path) {
      var list = path.split('/');
      var title = list[list.length - 1];

      $("#title").text('NURMAWADAH - '+title.replace('.php','').toUpperCase());
      $(this).addClass("active");
    }else{
      var list = path.split('/');
      var title_split = list[list.length - 1];
      var title = title_split.split('.php');

      $("#title").text('NURMAWADAH - '+title[0].toUpperCase());
    }
  });

  /**
   * Easy selector helper function
   */
  const select = (el, all = false) => {
    el = el.trim()
    if (all) {
      return [...document.querySelectorAll(el)]
    } else {
      return document.querySelector(el)
    }
  }

  /**
   * Easy event listener function
   */
  const on = (type, el, listener, all = false) => {
    let selectEl = select(el, all)
    if (selectEl) {
      if (all) {
        selectEl.forEach(e => e.addEventListener(type, listener))
      } else {
        selectEl.addEventListener(type, listener)
      }
    }
  }

  /**
   * Easy on scroll event listener 
   */
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
  }

  /**
   * Navbar links active state on scroll
   */
  let navbarlinks = select('#navbar .scrollto', true)
  const navbarlinksActive = () => {
    let position = window.scrollY + 200
    navbarlinks.forEach(navbarlink => {
      if (!navbarlink.hash) return
      let section = select(navbarlink.hash)
      if (!section) return
      if (position >= section.offsetTop && position <= (section.offsetTop + section.offsetHeight)) {
        navbarlink.classList.add('active')
      } else {
        navbarlink.classList.remove('active')
      }
    })
  }
  window.addEventListener('load', navbarlinksActive)
  onscroll(document, navbarlinksActive)

  /**
   * Scrolls to an element with header offset
   */
  const scrollto = (el) => {
    let elementPos = select(el).offsetTop
    window.scrollTo({
      top: elementPos,
      behavior: 'smooth'
    })
  }

  /**
   * Back to top button
   */
  let backtotop = select('.back-to-top')
  if (backtotop) {
    const toggleBacktotop = () => {
      if (window.scrollY > 100) {
        backtotop.classList.add('active')
      } else {
        backtotop.classList.remove('active')
      }
    }
    window.addEventListener('load', toggleBacktotop)
    onscroll(document, toggleBacktotop)
  }

  /**
   * Mobile nav toggle
   */
  on('click', '.mobile-nav-toggle', function(e) {
    select('body').classList.toggle('mobile-nav-active')
    this.classList.toggle('bi-list')
    this.classList.toggle('bi-x')
  })

  /**
   * Scrool with ofset on links with a class name .scrollto
   */
  on('click', '.scrollto', function(e) {
    if (select(this.hash)) {
      e.preventDefault()

      let body = select('body')
      if (body.classList.contains('mobile-nav-active')) {
        body.classList.remove('mobile-nav-active')
        let navbarToggle = select('.mobile-nav-toggle')
        navbarToggle.classList.toggle('bi-list')
        navbarToggle.classList.toggle('bi-x')
      }
      scrollto(this.hash)
    }
  }, true)

  /**
   * Scroll with ofset on page load with hash links in the url
   */
  window.addEventListener('load', () => {
    if (window.location.hash) {
      if (select(window.location.hash)) {
        scrollto(window.location.hash)
      }
    }
  });

  /**
   * Hero type effect
   */
  const typed = select('.typed')
  if (typed) {
    let typed_strings = typed.getAttribute('data-typed-items')
    typed_strings = typed_strings.split(',')
    new Typed('.typed', {
      strings: typed_strings,
      loop: true,
      typeSpeed: 100,
      backSpeed: 50,
      backDelay: 2000
    });
  }

  /**
   * Skills animation
   */
  let skilsContent = select('.skills-content');
  if (skilsContent) {
    new Waypoint({
      element: skilsContent,
      offset: '80%',
      handler: function(direction) {
        let progress = select('.progress .progress-bar', true);
        progress.forEach((el) => {
          el.style.width = el.getAttribute('aria-valuenow') + '%'
        });
      }
    })
  }

  /**
   * Porfolio isotope and filter
   */
  window.addEventListener('load', () => {
    let portfolioContainer = select('.portfolio-container');
    if (portfolioContainer) {
      let portfolioIsotope = new Isotope(portfolioContainer, {
        itemSelector: '.portfolio-item'
      });

      let portfolioFilters = select('#portfolio-flters li', true);

      on('click', '#portfolio-flters li', function(e) {
        e.preventDefault();
        portfolioFilters.forEach(function(el) {
          el.classList.remove('filter-active');
        });
        this.classList.add('filter-active');

        portfolioIsotope.arrange({
          filter: this.getAttribute('data-filter')
        });
        portfolioIsotope.on('arrangeComplete', function() {
          AOS.refresh()
        });
      }, true);
    }

  });

  /**
   * Initiate portfolio lightbox 
   */
  const portfolioLightbox = GLightbox({
    selector: '.portfolio-lightbox'
  });

  /**
   * Portfolio details slider
   */
  new Swiper('.portfolio-details-slider', {
    speed: 400,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    }
  });

  /**
   * Testimonials slider
   */
  new Swiper('.testimonials-slider', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },

      1200: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    }
  });

  /**
   * Animation on scroll
   */
  window.addEventListener('load', () => {
    AOS.init({
      duration: 1000,
      easing: 'ease-in-out',
      once: true,
      mirror: false
    })
  });

})()

/**
   * Approve Reject function
   */
function actionBook (id,status,action,customer_name) {
  $.ajax({
    type:'GET',
    url:'actionbookingdetail.php?id='+id+'&status='+status+'&action='+action+'&customer_name='+customer_name,
    success:function(data){
      if(data.status == true){
        swal.fire({
          title: 'Success!',
          text: data.message,
          icon: 'success',
          customClass: 'swal-wide'
        }).then(()=>{
          window.location = 'booking_detail.php';
        });
      }else{
        swal.fire({
          title: 'Failed!',
          text: data.message,
          icon: 'error',
          customClass: 'swal-wide'
        })
      }
    },
  });
}