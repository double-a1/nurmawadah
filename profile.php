<?php
    session_start();
    include 'connect.php';
    include 'session.php';

    $username = $_SESSION['username'];
    $sql = "SELECT * FROM user WHERE username='$username'";
    $result = mysqli_query($conn,$sql);
    $row = mysqli_fetch_array($result);

    $id = $row['id'];
    $username = $row['username'];
    $name = $row['name'];
    $image = $row['image'];

    preg_match( '/(\d{3})(\d{3})(\d{4})$/',$row['contact_no'],$matches);
    $contact_no = $matches[1] . '-' .$matches[2] . $matches[3];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href='assets/css/profile.css' rel='stylesheet'/>
</head>
<body>
  <?php include 'loading.php';?>
  <div id="body" style="display:none;">
    <?php include 'layout/header.php';?>
    <main id="main">
      <section class="breadcrumbs">
        <div class="container">
          <div class="container d-flex">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-user"></i><small> My Profile</small></a></li>
            </ul>
          </div>
        </div>
      </section>
      <section class="d-flex flex-column justify-content-center align-items-center">
        <center>
          <h2 class="page-title"><i class="bx bx-user"></i> My Profile </h2>
        </center>
        <div class="card">
          <?php if(empty($image)){ ?>
            <img style="width:300px;height:300px;" src="assets/img/avatar.png" style="width:100%">
          <?php }else{ ?>
            <img style="width:300px;height:300px;" src="assets/img/profile/<?php echo $image ?>" style="width:100%">
          <?php }?>
          <h2 style="margin-top:10px"><?php echo $name ?></h2>
          <ul class="list-group">
            <li class="list-group-item"><h4><span style="color:black" class="badge badge-dark">Username</span></h4><span><?php echo $username ?></span></li>
            <li class="list-group-item"><h4><span style="color:black" class="badge badge-dark">Contact No</span></h4><span><?php echo $contact_no ?></span></li>
          </ul>
          <div class="main">
            <div class="button-row">
              <div><a href="editprofile.php?id=<?php echo $id ?>" title="Edit Profile"></a></div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
    <?php include 'layout/footer.php';?>
  </div>
</body>
</html>