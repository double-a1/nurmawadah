<style>
    input:focus ~ label, textarea:focus ~ label, input:valid ~ label, textarea:valid ~ label {
        font-size: 0.75em;
        color: #999;
        top: -5px;
        -webkit-transition: all 0.225s ease;
        transition: all 0.225s ease;
    }

    .styled-input {
        float: left;
        width: 293px;
        margin: 1rem 0;
        position: relative;
        border-radius: 4px;
    }

    @media only screen and (max-width: 768px){
        .styled-input {
            width:100%;
        }
    }

    .styled-input label {
        color: #999;
        padding: 1.3rem 30px 1rem 30px;
        position: absolute;
        top: 10px;
        left: 0;
        -webkit-transition: all 0.25s ease;
        transition: all 0.25s ease;
        pointer-events: none;
    }

    .styled-input.wide { 
        width: 650px;
        max-width: 100%;
    }

    input,
    textarea {
        padding: 30px;
        border: 0;
        width: 100%;
        font-size: 1rem;
        background-color: #2d2d2d;
        color: white;
        border-radius: 4px;
    }

    input:focus,
    textarea:focus { outline: 0; }

    input:focus ~ span,
    textarea:focus ~ span {
        width: 100%;
        -webkit-transition: all 0.075s ease;
        transition: all 0.075s ease;
    }

    textarea {
        width: 100%;
        min-height: 15em;
    }

    .input-container {
        width: 650px;
        max-width: 100%;
        margin: 20px auto 25px auto;
    }

    .submit-btn {
        float: right;
        padding: 7px 35px;
        border-radius: 60px;
        display: inline-block;
        background-color: #4b8cfb;
        color: white;
        font-size: 18px;
        cursor: pointer;
        box-shadow: 0 2px 5px 0 rgba(0,0,0,0.06),
                0 2px 10px 0 rgba(0,0,0,0.07);
        -webkit-transition: all 300ms ease;
        transition: all 300ms ease;
    }

    .submit-btn:hover {
        transform: translateY(1px);
        box-shadow: 0 1px 1px 0 rgba(0,0,0,0.10),
                0 1px 1px 0 rgba(0,0,0,0.09);
    }

    @media (max-width: 768px) {
        .submit-btn {
            width:100%;
            float: none;
            text-align:center;
        }
    }

    input[type=checkbox] + label {
    color: #ccc;
    font-style: italic;
    } 

    input[type=checkbox]:checked + label {
    color: #f00;
    font-style: normal;
    }
</style>
<?php include 'layout/head.php';?>
<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<section class="breadcrumbs">
    <div class="container">
        <div class="container d-flex">
            <ul class="pagination" style="padding-left: 425px; text-align: center">
                <li class="page-item"><h4>NURMAWADAH MUSLIMAH HAIR SALOON</h4></li>
            </ul>
            <ul class="pagination" style="padding-left: 250px;">
                <li class="page-item active"><a class="page-link" href="login.php"><small> LOGIN</small></a></li>
            </ul>
        </div>
    </div>
</section>
<div id="body" style="display:none;">
    <section class="flex-column justify-content-center align-items-center">
        <div id="form-container" style="width: 60%;margin-left: 330px;">
            <h1 class="page-title"><i class="bx bx-file"></i> Ask Question </h1>
            <div class="description">
                <div class="form-group row">
                    <label for="item_name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" autocomplete="off" name="name" id="name">
                    </div>
                </div>

                <div class="form-group row"><div class="col-sm-12"><br></div></div>

                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" autocomplete="off" name="email" id="email">
                    </div>
                </div>

                <div class="form-group row"><div class="col-sm-12"><br></div></div>

                <div class="form-group row">
                    <label for="quantity" class="col-sm-2 col-form-label">Question</label>
                    <div class="col-sm-10">
                        <textarea rows="5" class="form-control" name="question" id="question"></textarea>
                    </div>
                </div>
            </div>
            <footer>
                <center>
                    <div class="button-row" style="margin-right: 300px;">
                        <div><a id="btnAdd" title="Add Question"></a></div>
                    </div>
                </center>
            </footer>
        </div>
    </section>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/sweetalert/main.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
        $('#btnAdd').click(function(){
            obj = {
                name:$('#name').val(),
                email:$('#email').val(),
                question:$('#question').val(),
            };
            
            $.ajax({
                type:'POST',
                url:'actionfeedback.php?action=add',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'index.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });
    </script>
</div>
