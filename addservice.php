<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                            <li class="page-item"><a class="page-link" href="service.php"><i class="bx bxs-shopping-bag"></i><small> Service</small></a></li>
                            <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-plus"></i><small> Add Service</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="d-flex flex-column justify-content-center align-items-center">
                <h2 class="page-title"><i class="bx bxs-shopping-bag"></i> Add Service </h2>
                <div class="container">
                    <div class="card">
                        <div class="card-body">
                            <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
                            <div class="form-group row">
                                <label for="service_type" class="col-sm-2 col-form-label">Service Type</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="service_type" id="service_type">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="titles" class="col-sm-2 col-form-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="titles" id="titles">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="description" class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="description" id="description">
                                    <small> Input by commas ',' (i.e. Item1,Item2,Item3)</small>
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-2 col-form-label">Price</label>
                                <div class="col-sm-10">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">RM</span>
                                        <input type="text" class="form-control" autocomplete="off" name="price" id="price" onkeypress="return isNumberKey(this);">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="color" class="col-sm-2 col-form-label">Color</label>
                                <div class="col-sm-2">
                                    <input type="color" class="form-control form-control-color" autocomplete="off" name="color" id="color">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <center>
                                <div class="button-row" style="margin-right: 300px;">
                                    <div><a id="btnAdd" title="Add Service"></a></div>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $('#btnAdd').click(function(){
            obj = {
                id:$('#id').val(),
                service_type:$('#service_type').val(),
                title:$('#titles').val(),
                description:$('#description').val(),
                price:$('#price').val(),
                color:$('#color').val()
            };
            
            $.ajax({
                type:'POST',
                url:'actionservice.php?action=add',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'service.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode
            console.log(charCode);
            if ((charCode > 47 && charCode < 58) || charCode == 46) {
                return true;
            }
            return false;
        }
    </script>
</body>
</html>