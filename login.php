<!DOCTYPE html>
<html>
<head>
	<title>NURMAWADAH - LOGIN</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<link href="assets/img/favicon.png" rel="icon">
	<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
	<link href="assets/vendor/fontawesome/css/all.min.css" rel="stylesheet">
	<link href="assets/css/index.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<div class="top"></div>
		<div class="bottom"></div>
		<div class="center">
			<h4>NURMAWADAH MUSLIMAH HAIR SALOON</h4>
			<h3>SIGN IN</h3>
			<form name="form-login" action="login.php" method="POST">
				<input type="text" name="username" placeholder="Username" autocomplete="off">
				<input type="password" name="password" placeholder="Password" autocomplete="off">
				<button name="login">LOG IN</button>
				<div id="sign_up">
					<center><a href="register.php">Sign Up as Member</a></center>
				</div>
				<div id="fgt_pswd">
					<center><a href="forgotpassword.php">Forgot password?</a></center>
				</div>
			</form>
		</div>
	</div>
	<script src="assets/vendor/sweetalert/main.min.js"></script>
	<script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })
    </script>
</body>
</html>
<?php
	include 'connect.php';
	session_start();

	if(isset($_POST['login'])){
		$username= $_POST['username'];
		$password = md5($_POST['password']);

		$sql = "SELECT * FROM user WHERE username='$username' AND password = '$password'";
		$result = mysqli_query($conn, $sql);
		$row = mysqli_fetch_array($result);
		
		$sq = "SELECT * FROM user WHERE username ='$username' AND password = '$password'";
		$result1 = mysqli_query($conn, $sq);
		$check_user = mysqli_num_rows($result1);
		
		if($check_user == 1) {
            $_SESSION["username"] = $row['username'];
			$_SESSION["name"] = $row['name'];
			$_SESSION["contact_no"] = $row['contact_no'];
			$_SESSION["user_type"] = $row['user_type'];
			header("Location: home.php");
			exit();
        } else {
			$msg = mysqli_error($conn);
            echo "	<script src='//cdn.jsdelivr.net/npm/sweetalert2@11'></script><script>
						Toast.fire({
							text: 'Invalid Username or Password!',
							icon: 'error',
							position: 'top-end',
						})
					</script>";
        }
	}
?>