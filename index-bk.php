<?php
    include 'connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href="assets/css/product.css" rel="stylesheet">
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <main>
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination" style="padding-left: 425px; text-align: center">
                            <li class="page-item"><h4>NURMAWADAH MUSLIMAH HAIR SALOON</h4></li>
                        </ul>
						<ul class="pagination" style="padding-left: 120px;">
                            <li class="page-item active"><a class="page-link" href="feedback.php"><small> ASK US</small></a></li>
                            <li class="page-item active"><a class="page-link" href="login.php"><small> LOGIN</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="flex-column justify-content-center align-items-center">
                <div id="modal-container">
                    <div class="description">
                        <center>
                            <div class="center">
                                <button class="fancy">
                                    <span class="top-key"></span>
									<a href="addproduct.php">Checkout</a>
                                    <span class="bottom-key-1"></span>
                                    <span class="bottom-key-2"></span>
                                </button>
                            </div>
                            <div class="container mt-2 px-5 py-5">
                                <div class="row">
                                    <?php
                                        $sql = "SELECT * FROM inventory";
                                        $statement = $conn->query($sql);
                                        $result = $statement->fetch_all(MYSQLI_ASSOC);
                                    
                                        foreach($result as $row){
                                            if(intval($row['quantity']) == 0){
                                                $item_status = 'Out of stock';
                                                $action = '<ul class="product-links">
                                                                <li><a href="actionproduct.php?id='.$row["id"].'&price='.$row["price"].'&action=deleteCart"><i class="bx bx-minus"></i></a></li>
                                                            </ul>';
                                            }else{
                                                $item_status = '';
                                                $action = '<ul class="product-links">
                                                                <li><a href="actionproduct.php?id='.$row["id"].'&price='.$row["price"].'&action=deleteCart"><i class="bx bx-minus"></i></a></li>
                                                                <li><a href="actionproduct.php?id='.$row["id"].'&item_name='.$row["item_name"].'&price='.$row["price"].'&action=add"><i class="bx bx-plus"></i></a></li>
                                                            </ul>';
                                            }
                                            echo    '<div class="col-md-3 col-sm-6" style="margin-bottom: 30px;">
                                                        <div class="product-grid">
                                                            <div class="product-image">
                                                                <a href="javascript:void(0)" class="image">
                                                                    <img class="pic-1" style="margin-top: 20px; width:125px !important; height:150px !important" src="assets/img/inventory/'.$row["image"].'">
                                                                    <img class="pic-2" style="margin-top: 20px; width:125px !important; height:150px !important; left: 20%;" src="assets/img/inventory/'.$row["image"].'">
                                                                </a>
                                                                '.$action.'
                                                            </div>
                                                            <div class="product-content" style="height:180px !important">
                                                                <div style="margin-bottom:10px" class="rating" data-rateyo-rating="'.$row["rating"].'"></div>
                                                                <h3 class="title"><a href="javascript:void(0)">'.$row["item_name"].'</a></h3>
                                                                <div class="price">RM '.$row["price"].'</div>
                                                                <div>Quantity: '.$row["quantity"].'</div>
                                                                <div style="color:red;font-weight:bold">'.$item_status.'</div>
                                                            </div>
                                                        </div>
                                                    </div>';
                                        }
                                    ?>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
		<!-- Vendor JS Files -->
		<script src="assets/vendor/jquery/jquery.min.js"></script>
		<script src="assets/vendor/datatable/datatables.min.js" type="text/javascript"></script>
		<script src="assets/vendor/fullcalendar/js/main.min.js"></script>
		<script src="assets/vendor/sweetalert/main.min.js"></script>
		<script src="assets/vendor/aos/aos.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
		<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
		<script src="assets/vendor/php-email-form/validate.js"></script>
		<script src="assets/vendor/purecounter/purecounter.js"></script>
		<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
		<script src="assets/vendor/typed.js/typed.min.js"></script>
		<script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
		<script src="assets/vendor/calculator/js/calculate.js"></script>
		<script src="assets/vendor/rateyo/jquery.rateyo.min.js"></script>
		<script src="assets/vendor/snap/snap.js"></script>
		<script src="assets/vendor/plotly/plotly-2.16.1.min.js"></script>
		<script src="assets/js/main.js"></script>
	</div>
    <script>
        $(".rating").rateYo({
            starWidth: "20px",
            readOnly: true
        });
    </script>
</body>
</html>