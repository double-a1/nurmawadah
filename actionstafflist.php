<?php
	header('Content-Type: application/json');

	include 'connect.php';

	$action = ( isset($_GET["action"]) && !empty($_GET["action"]) ) ? $_GET["action"] : 'table';

	switch($action) {
        case 'add' :
			$username = $_POST["username"];
            $name = $_POST["name"];
			$contact_no = $_POST["contact_no"];
            $user_type = $_POST["user_type"];
			$password = md5('123');

			$query = "INSERT INTO user (
				username,
                name,
				contact_no, 
				user_type,
                password,
				updated_date)
			VALUES (
				'$username',
                '$name',
				'$contact_no',
				'$user_type',
                '$password',
				CURRENT_TIMESTAMP)";

			$query_cron = "INSERT INTO attendance (
				username,
				user_type,
				checkin_status,
				checkout_status, 
				updated_date)
			VALUES (
				'$username',
				'$user_type',
				'0',
				'0',
				CURRENT_TIMESTAMP)";

			mysqli_query($conn, $query_cron);
			
			if(mysqli_query($conn, $query)){
				$msg = "Staff added successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'edit':
			$id = $_POST['id'];
			$username = $_POST["username"];
			$name = $_POST["name"];
			$contact_no = $_POST["contact_no"];
            $user_type = $_POST["user_type"];

			$query = "UPDATE user SET 
				username = '$username',
                name = '$name',
				contact_no = '$contact_no',
                user_type = '$user_type',
				updated_date = CURRENT_TIMESTAMP
				WHERE id = '$id'";
			
			if(mysqli_query($conn, $query)){
				$msg = "Staff edited successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'delete':
			$id = $_POST['id'];
			
			$query = "DELETE FROM user WHERE id = '$id'";
			if(mysqli_query($conn, $query)){
				$msg = "Staff deleted successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}
			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'getStaff':
			$id = $_GET["id"];

			$sql = "SELECT * FROM user WHERE id='$id'";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_array($result);

			$data = array(
				'id'        => $row["id"],
				'username'  => $row["username"],
                'name'      => $row["name"],
                'contact_no'=> $row["contact_no"],
				'user_type' => $row["user_type"]
			);

			echo json_encode($data);
		break;

		default:
			$sql = "SELECT * FROM user WHERE user_type IN ('admin', 'staff')";
			$statement = $conn->query($sql);
			$result = $statement->fetch_all(MYSQLI_ASSOC);
			
			$data = [];
			foreach($result as $key => $row){
				$data[] = array(
                    'key'        => $key+1,
					'id'         => $row["id"],
					'username'   => $row["username"],
                    'name'       => $row["name"],
					'contact_no' => $row["contact_no"],
					'user_type'  => $row["user_type"],
				);
			}
			echo json_encode($data);
		break;
	}
 ?>