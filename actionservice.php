<?php
	header('Content-Type: application/json');

	include 'connect.php';

	$action = $_GET["action"];
	$id = $_POST["id"];

	switch($action) {
		case 'add' :
			$service_type = $_POST["service_type"];
			$title = $_POST["title"];
			$description = $_POST["description"];
			$price = $_POST["price"];
			$color = $_POST["color"];

			$query = "INSERT INTO service (
				service_type, 
				title, 
				description,
				price, 
				color,
				updated_date)
			VALUES (
				'$service_type', 
				'$title', 
				'$description', 
				'$price', 
				'$color',
				CURRENT_TIMESTAMP)";
			
			if(mysqli_query($conn, $query)){
				$msg = "Service added successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'edit':
			$service_type = $_POST["service_type"];
			$title = $_POST["title"];
			$description = $_POST["description"];
			$price = $_POST["price"];
			$color = $_POST["color"];
			
			$query = "UPDATE service SET 
				service_type = '$service_type', 
				title = '$title', 
				description = '$description', 
				price = '$price', 
				color = '$color',
				updated_date = CURRENT_TIMESTAMP
				WHERE id = '$id'";
			
			if(mysqli_query($conn, $query)){
				$msg = "Service edited successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

        case 'delete':
			$query = "DELETE FROM service WHERE id = '$id'";
			if(mysqli_query($conn, $query)){
				$msg = "Service deleted successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}
			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		default:
		break;
	}
 ?>