<?php
    session_start();
    include 'connect.php';
    include 'session.php';

    $id = $_GET["id"];

    $sql = "SELECT * FROM service WHERE id='$id'";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($result);

    $service_type = $row["service_type"];
    $title = $row["title"];
    $description = $row["description"];
    $price = $row["price"];
    $color = $row["color"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                            <li class="page-item"><a class="page-link" href="service.php"><i class="bx bxs-shopping-bag"></i><small> Service</small></a></li>
                            <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-pencil"></i><small> Edit Service</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="d-flex flex-column justify-content-center align-items-center">
                <h2 class="page-title"><i class="bx bxs-shopping-bag"></i> Edit Service </h2>
                <div class="container">
                    <div class="card">
                        <div class="card-body">
                            <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
                            <div class="form-group row">
                                <label for="service_type" class="col-sm-2 col-form-label">Service Type</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="service_type" id="service_type" value="<?php echo $service_type ?>">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="titles" class="col-sm-2 col-form-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="titles" id="titles" value="<?php echo $title ?>">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="description" class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="description" id="description" value="<?php echo $description ?>">
                                    <small> Input by commas ',' (i.e. Item1,Item2,Item3)</small>
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-2 col-form-label">Price</label>
                                <div class="col-sm-10">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">RM</span>
                                        <input type="text" class="form-control" autocomplete="off" name="price" id="price" value="<?php echo $price ?>" onkeypress="return isNumberKey(this);">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="color" class="col-sm-2 col-form-label">Color</label>
                                <div class="col-sm-2">
                                    <input type="color" class="form-control form-control-color" autocomplete="off" name="color" id="color" value="<?php echo $color ?>">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <center>
                                <div class="button-row" style="margin-right: 250px;">
                                    <div class="modal-button-save"><a id="btnEdit" title="Edit Service"></a></div>
                                </div>
                                <div class="button-row" style="margin-right: 250px;">
                                    <div class="modal-button-delete"><a id="btnDelete" title="Delete Service"></a></div>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $('#btnEdit').click(function(){
            obj = {
                id:$('#id').val(),
                service_type:$('#service_type').val(),
                title:$('#titles').val(),
                description:$('#description').val(),
                price:$('#price').val(),
                color:$('#color').val()
            };
            
            $.ajax({
                type:'POST',
                url:'actionservice.php?action=edit',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'service.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        $('#btnDelete').click(function(){
            obj = {
                id:$('#id').val()
            };
            
            $.ajax({
                type:'POST',
                url:'actionservice.php?action=delete',
                data:obj,
                success:function(data){
                    console.log(data);
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'service.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode
            console.log(charCode);
            if ((charCode > 47 && charCode < 58) || charCode == 46) {
                return true;
            }
            return false;
        }
    </script>
</body>
</html>