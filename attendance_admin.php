<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href="assets/css/transaction.css" rel="stylesheet">
    <link href="assets/css/customerlist.css" rel="stylesheet">
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
            <div class="container">
                <div class="container d-flex">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                    <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-time"></i><small> Attendance List</small></a></li>
                </ul>
                </div>
            </div>
            </section>
            <section class="d-flex flex-column justify-content-center align-items-center">
                <center>
                    <h2 class="page-title"><i class="bx bx-time"></i> Attendance List </h2>
                </center>
                <div class="button-row" style="margin-right: 200px;">
                    <div class="modal-button-delete"><a id="btnReset" title="Reset"></a></div>
                </div>
                <div class="container mt-5 px-2">
                    <div class="table-responsive">
                        <table id="attendance" class="table table-striped table-bordered">
                            <thead class="table-dark">
                                <tr>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">No</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Staff ID</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Check In</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Check Out</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $('#btnReset').click(function(){            
            $.ajax({
                type:'POST',
                url:'actionattendance.php?action=reset',
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'attendance_admin.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });
    </script>
</body>
</html>