<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "nurmawadah";

    //Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);

    //Check connection
    if (!$conn) {
        die("Connection failed: ". mysqli_connect_error());
    }

    $query = "UPDATE attendance SET
        checkin_status = '0', 
        checkout_status = '0',
        updated_date = CURRENT_TIMESTAMP
        WHERE user_type = 'staff'";

    mysqli_query($conn, $query);
    $msg = "Reset attendance cron run successfully!";
    $status = true;

    $cron = "INSERT INTO cron (
                    date,
                    status,
                    updated_date)
                VALUES(
                    date = CURRENT_TIMESTAMP,
                    status = '$msg',
                    updated_date = CURRENT_TIMESTAMP)";

    mysqli_query($conn, $cron);

    $data = array("status"=>$status, "message"=>$msg);
    echo json_encode($data);
 ?>