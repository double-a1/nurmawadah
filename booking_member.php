<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href='assets/css/booking.css' rel='stylesheet'/>
</head>
<body>
  <?php include 'loading.php';?>
  <div id="body" style="display:none;">
    <?php include 'layout/header.php';?>
    <main id="main">
      <section class="breadcrumbs">
        <div class="container">
          <div class="container d-flex">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-calendar"></i><small> Booking</small></a></li>
            </ul>
          </div>
        </div>
      </section>
      <section class="flex-column justify-content-center align-items-center">
        <div class="container">
          <center>
            <h2 class="page-title"><i class="bx bx-calendar"></i> Booking </h2>
          </center>
          <div class="col-md-12">
            <div id='calendar'></div>
          </div>
          <div style="margin-top:2%;">
            <?php
              $sql = "SELECT * FROM service";
              $statement = $conn->query($sql);
              $result = $statement->fetch_all(MYSQLI_ASSOC);

              foreach($result as $row){
                echo '<span style="color:'.$row["color"].'"><i style="margin-left: 15px;" class="fas fa-circle fa-sm"></i></span><b> '.$row["title"].'</b>';
              }
            ?>
            <span style="color:#000000"><i style="margin-left: 15px;" class="fas fa-circle fa-sm"></i></span><b> Multiple Package</b>
          </div>
        </div>
      </section>
    </main>

    <!-- Add Booking -->
    <div id="AddBooking" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Add Booking</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <i style="color:white" class="fas fa-times-circle"></i>
            </button>
          </div>
          <div class="modal-body">
            <div id="modal-container">
              <div class="description">
                <div class="form-group row">
                  <label for="date" class="col-sm-3 col-form-label">Date / Time</label>
                  <div class="col-sm-5">
                    <input type="date" class="form-control" name="date" id="date" readonly>
                  </div>
                  <div class="col-sm-4">
                    <input type="time" class="form-control" name="time" id="add_time">
                  </div>
                </div>

                <div class="form-group row"><div class="col-sm-12"><br></div></div>

                <div class="form-group row">
                  <label for="customer_name" class="col-sm-3 col-form-label">Customer Name</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" autocomplete="off" id="add_customer_name" name="add_customer_name"  placeholder="Name" value="<?php echo $name ?>"/>
                  </div>
                </div>

                <div class="form-group row"><div class="col-sm-12"><br></div></div>

                <div class="form-group row">
                  <label for="contact_no" class="col-sm-3 col-form-label">Contact No</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" autocomplete="off" id="contact_no" name="contact_no" placeholder="Contact No" value="<?php echo $contact_no ?>">
                  </div>
                </div>

                <div class="form-group row"><div class="col-sm-12"><br></div></div>

                <div class="form-group row">
                  <label for="service_type" class="col-sm-3 col-form-label">Service Package</label>
                  <div class="col-sm-9">
                    <div class="selectBox" onclick="showCheckboxes()">
                      <select class="form-select" id="service_type">
                        <option>Select service package</option>
                      </select>
                      <div class="overSelect"></div>
                    </div>
                    <div id="checkboxes">
                      <?php
                        $sql = "SELECT * FROM service";
                        $statement = $conn->query($sql);
                        $result = $statement->fetch_all(MYSQLI_ASSOC);

                        foreach($result as $row){
                          $id_key = str_replace(" ","",$row["service_type"]);
                          echo '<label><input type="checkbox" name="service_type[]" id="'.$id_key.'" value="'.$row["service_type"].'"/> '.$row["service_type"].' - '.$row["title"].'</label>';
                        }
                      ?>
                    </div>
                  </div>
                </div>

                <div class="form-group row"><div class="col-sm-12"><br></div></div>

                <div class="form-group row">
                  <label for="remarks" class="col-sm-3 col-form-label">Remarks</label>
                  <div class="col-sm-9">
                    <textarea rows="5" class="form-control" name="remarks" id="remarks"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              <center>
                <div class="button-row" style="margin-right: 400px;">
                  <div class="modal-button-save"><a id="btnAdd" title="Add Booking"></a></div>
                </div>
              </center>
          </div>
        </div>
      </div>
    </div>
    <!-- End Add Booking -->

    <!-- Edit Booking -->
    <div id="EditBooking" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form enctype='multipart/form-data' method="POST" action="" class="well form-horizontal">
            <div class="modal-header">
              <h5 class="modal-title">Edit Booking</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i style="color:white" class="fas fa-times-circle"></i>
              </button>
            </div>
            <div class="modal-body">
              <div id="modal-container">
                <div class="description">
                  <input type="hidden" name="id" id="id">
                  <div class="form-group row">
                    <label for="date" class="col-sm-3 col-form-label">Date / Time</label>
                    <div class="col-sm-5">
                      <input type="date" class="form-control" name="date" id="date">
                    </div>
                    <div class="col-sm-4">
                      <input type="time" class="form-control" name="time" id="time">
                    </div>
                  </div>

                  <div class="form-group row"><div class="col-sm-12"><br></div></div>

                  <div class="form-group row">
                    <label for="customer_name" class="col-sm-3 col-form-label">Customer Name</label>
                    <div class="col-sm-9">
                      <input class="form-control" autocomplete="off" type="text" id="edit_customer_name" name="customer_name" list="customer_name"/>
                      <datalist id="customer_name">
                        <?php
                          $sql = "SELECT * FROM customer";
                          $statement = $conn->query($sql);
                          $result = $statement->fetch_all(MYSQLI_ASSOC);

                          foreach($result as $row){
                            echo '<option value="'.$row["customer_name"].'">'.$row["customer_name"].'</option>';
                          }
                        ?>
                      </datalist>
                    </div>
                  </div>

                  <div class="form-group row"><div class="col-sm-12"><br></div></div>

                  <div class="form-group row">
                    <label for="contact_no" class="col-sm-3 col-form-label">Contact No</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" autocomplete="off" name="contact_no" id="contact_no">
                    </div>
                  </div>

                  <div class="form-group row"><div class="col-sm-12"><br></div></div>

                  <div class="form-group row">
                    <label for="service_type" class="col-sm-3 col-form-label">Service Package</label>
                    <div class="col-sm-9">
                      <div class="selectBoxEdit" onclick="showCheckboxesEdit()">
                        <select class="form-select" id="service_type">
                          <option>Select service package</option>
                        </select>
                        <div class="overSelectEdit"></div>
                      </div>
                      <div id="checkboxesEdit" style="display:block">
                        <?php
                          $sql = "SELECT * FROM service";
                          $statement = $conn->query($sql);
                          $result = $statement->fetch_all(MYSQLI_ASSOC);

                          foreach($result as $row){
                            $id_key = str_replace(" ","",$row["service_type"]);
                            echo '<label><input type="checkbox" name="service_type[]" id="'.$id_key.'" value="'.$row["service_type"].'"/> '.$row["service_type"].' - '.$row["title"].'</label>';
                          }
                        ?>
                      </div>
                    </div>
                  </div>

                  <div class="form-group row"><div class="col-sm-12"><br></div></div>

                  <div class="form-group row">
                    <label for="remarks" class="col-sm-3 col-form-label">Remarks</label>
                    <div class="col-sm-9">
                      <textarea rows="5" class="form-control" name="remarks" id="remarks"></textarea>
                    </div>
                  </div>

                  <div class="form-group row"><div class="col-sm-12"><br></div></div>

                  <?php if ($user_type != 'member') { ?>
                    <div class="form-group row" id="statusDiv" style="display: none">
                      <label for="status" class="col-sm-3 col-form-label">Status Booking</label>
                      <div class="col-sm-4">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="progress" value="progress">
                          <label class="form-check-label">
                            In Progress
                          </label>
                        </div>
                      </div>

                      <div class="col-sm-4">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="status" id="complete" value="complete">
                          <label class="form-check-label">
                            Complete
                          </label>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                </div>
              </div> 
            </div>
            <div class="modal-footer">
              <center>
                <div class="button-row" style="margin-right: 250px;">
                  <div class="modal-button-save"><a id="btnEdit" title="Edit Booking"></a></div>
                </div>
                <div class="button-row" style="margin-right: 250px;">
                  <div class="modal-button-delete"><a id="btnDelete" title="Delete Booking"></a></div>
                </div>
              </center>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- End Edit Booking -->

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
    <?php include 'layout/footer.php';?>
  </div>
  <script>
    $('#btnAdd').click(function(){
      var list = $("#AddBooking input[name='service_type[]']").map(function(){
        if($(this).prop("checked") == true){
          return $(this).val();
        }
      }).get();
      var service_type = list.join();
      NewEvent = {
        customer_name:$('#AddBooking #add_customer_name').val(),
        contact_no:$('#AddBooking #contact_no').val(),
        title:'Booking - ' + $('#AddBooking #add_customer_name').val() + ' [PENDING]',
        service_type:service_type,
        remarks:$('#AddBooking #remarks').val(),
        start_date:$('#AddBooking #date').val()+" "+$('#AddBooking #add_time').val(),
        end_date:$('#AddBooking #date').val()+" "+$('#AddBooking #add_time').val(),
        status: 'pending',
      };
      SendInformation('add',NewEvent);
    });

    $('#btnEdit').click(function(){
      var list = $("#EditBooking input[name='service_type[]']").map(function(){
        if($(this).prop("checked") == true){
          return $(this).val();
        }
      }).get();
      var service_type = list.join();
      NewEvent = {
        id:$('#EditBooking #id').val(),
        customer_name:$('#EditBooking #edit_customer_name').val(),
        contact_no:$('#EditBooking #contact_no').val(),
        title:'Booking - ' + $('#EditBooking #edit_customer_name').val(),
        service_type:service_type,
        remarks:$('#EditBooking #remarks').val(),
        start_date:$('#EditBooking #date').val()+" "+$('#EditBooking #time').val(),
        end_date:$('#EditBooking #date').val()+" "+$('#EditBooking #time').val(),
        status:$("#EditBooking input[name='status']:checked").val(),
      };
      SendInformation('edit',NewEvent);
    });

    $('#btnDelete').click(function(){
      NewEvent = {
        id:$('#EditBooking #id').val()
      };
      SendInformation('delete',NewEvent);
    });

    function SendInformation(action,objEvent){
      $.ajax({
        type:'POST',
        url:'actionbooking.php?action='+action,
        data:objEvent,
        success:function(data){
          if(data.status == true){
            swal.fire({
              title: 'Success!',
              text: data.message,
              icon: 'success',
              customClass: 'swal-wide'
            }).then(()=>{
              location.reload();
            });
          }else{
            swal.fire({
              title: 'Failed!',
              text: data.message,
              icon: 'error',
              customClass: 'swal-wide'
            })
          }
        },
      });
    }

    $('.close').click(function(){
      $('.modal').modal('hide');
    });

    $('#add_customer_name').change(function(){
      var customer_name = $(this).val();
      $.ajax({
        type:'POST',
        url:'actioncustomerlist.php?action=checkUser',
        data:{customer_name:customer_name},
        success:function(data){
          $('#AddBooking #contact_no').val(data);
        }
      });
    });

    $('#edit_customer_name').change(function(){
      var customer_name = $(this).val();
      $.ajax({
        type:'POST',
        url:'actioncustomerlist.php?action=checkUser',
        data:{customer_name:customer_name},
        success:function(data){
          $('#EditBooking #contact_no').val(data);
        }
      });
    });

    $('#add_time').change(function(){
      var date = $('#AddBooking #date').val()+" "+$('#AddBooking #add_time').val();
      $.ajax({
        type:'POST',
        url:'actionbooking.php?action=checkDate',
        data:{date:date},
        success:function(data){
          if(data.status == false){
            swal.fire({
              title: 'Failed!',
              text: data.message,
              icon: 'error',
              customClass: 'swal-wide'
            })
          }
        }
      });
    });

    $('#EditBooking #time').change(function(){
      var date = $('#EditBooking #date').val()+" "+$('#EditBooking #time').val();
      $.ajax({
        type:'POST',
        url:'actionbooking.php?action=checkDate',
        data:{date:date},
        success:function(data){
          if(data.status == false){
            swal.fire({
              title: 'Failed!',
              text: data.message,
              icon: 'error',
              customClass: 'swal-wide'
            })
          }
        }
      });
    });

    var expanded = false;
    function showCheckboxes() {
      var checkboxes = document.getElementById("checkboxes");
      if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
      } else {
        checkboxes.style.display = "none";
        expanded = false;
      }
    }

    var expandedEdit = true;
    function showCheckboxesEdit() {
      var checkboxes = document.getElementById("checkboxesEdit");
      if (!expandedEdit) {
        checkboxes.style.display= "block";
        expandedEdit = true;
      } else {
        checkboxes.style.display = "none";
        expandedEdit = false;
      }
    }
  </script>
</body>
</html>