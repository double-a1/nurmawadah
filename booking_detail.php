<?php
    session_start();
    include 'connect.php';
    include 'session.php';

    $query = "UPDATE user SET 
				booking_status = '0',
				updated_date = CURRENT_TIMESTAMP
				WHERE user_type IN ('admin', 'staff')";
    
    mysqli_query($conn, $query);

    $sql = "SELECT * FROM user WHERE username='$username'";
    $result = mysqli_query($conn,$sql);
    $row = mysqli_fetch_array($result);
    
    $booking = $row['booking_status'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href="assets/css/transaction.css" rel="stylesheet">
    <link href="assets/css/customerlist.css" rel="stylesheet">
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                            <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-table"></i><small> Booking Detail List</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="flex-column justify-content-center align-items-center">
                <div id="modal-container">
                    <center>
                        <h1 class="page-title"><i class="bx bx-table"></i> Booking Detail List </h1>
                    </center>
                    <div class="description">
                        <div class="container mt-5 px-2">
                            <div class="table-responsive">
                                <table id="bookinglist" class="table table-striped table-bordered">
                                    <thead class="table-dark">
                                        <tr>
                                            <th scope="col" style="text-align:center;vertical-align: middle;">No</th>
                                            <th scope="col" style="text-align:center;vertical-align: middle;">Booking Date</th>
                                            <th scope="col" style="text-align:center;vertical-align: middle;">Title</th>
                                            <th scope="col" style="text-align:center;vertical-align: middle;">Service Type</th>
                                            <th scope="col" style="text-align:center;vertical-align: middle;">Status</th>
                                            <th scope="col" style="text-align:center;vertical-align: middle;">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
</body>
</html>