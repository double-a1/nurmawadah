<?php
	header('Content-Type: application/json');

	session_start();
	include 'connect.php';

	$name = $_SESSION['name'];
	$address = $_POST["address"];
	$payment_method = $_POST["payment_method"];
	$item_id = $_POST["item_id"];
	$total_price = $_POST["total_price"];
	$status = 'complete';

	$query = "INSERT INTO payment (
		customer_name, 
		address, 
		payment_method,
		item_id,
		total_price, 
		status,
		updated_date)
	VALUES (
		'$name', 
		'$address', 
		'$payment_method', 
		'$item_id',
		'$total_price', 
		'$status',
		CURRENT_TIMESTAMP)";

	if(mysqli_query($conn, $query)){
		$last_id = mysqli_insert_id($conn);
		$query = "UPDATE cart SET 
				status = 'complete',
				updated_date = CURRENT_TIMESTAMP
				WHERE customer_name='$name'";
		$msg = "Payment successfully!";
		mysqli_query($conn, $query);
		$status = true;
	}else{
		$msg = mysqli_error($conn);
		$status = false;
	}

	$data = array("status"=>$status, "message"=>$msg, "id"=>$last_id);
	echo json_encode($data);
 ?>