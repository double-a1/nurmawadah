<?php
	header('Content-Type: application/json');

	include 'connect.php';

	$action = ( isset($_GET["action"]) && !empty($_GET["action"]) ) ? $_GET["action"] : 'table';

	switch($action) {
		case 'checkin' :
            $username = $_POST["username"];

			$query = "UPDATE attendance SET
				checkin = CURRENT_TIMESTAMP, 
				checkin_status = '1',
				updated_date = CURRENT_TIMESTAMP
				WHERE username = '$username'";
			
			if(mysqli_query($conn, $query)){
				$msg = "Check In successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'checkout':
            $username = $_POST["username"];

			$query = "UPDATE attendance SET 
				checkout = CURRENT_TIMESTAMP, 
				checkout_status = '1',
				updated_date = CURRENT_TIMESTAMP
				WHERE username = '$username'";
			
			if(mysqli_query($conn, $query)){
				$msg = "Check Out successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

        case 'reset':
			$query = "UPDATE attendance SET
				checkin_status = '0', 
				checkout_status = '0',
				updated_date = CURRENT_TIMESTAMP
                WHERE user_type = 'staff'";

            if(mysqli_query($conn, $query)){
				$msg = "Attendance reset successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		default:
			$sql = "SELECT * FROM attendance where user_type = 'staff'";
			$statement = $conn->query($sql);
			$result = $statement->fetch_all(MYSQLI_ASSOC);
			
			$data = [];
			foreach($result as $key => $row){
				$data[] = array(
                    'key'        => $key+1,
					'id'         => $row["id"],
					'username'   => $row["username"],
                    'checkin'    => $row["checkin"],
					'checkout'   => $row["checkout"],
				);
			}
			echo json_encode($data);
		break;
	}
 ?>