<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href="assets/css/transaction.css" rel="stylesheet">
    <link href="assets/css/customerlist.css" rel="stylesheet">
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                            <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-file"></i><small> Feedback List</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="flex-column justify-content-center align-items-center">
                <div id="modal-container">
                    <center>
                        <h1 class="page-title"><i class="bx bx-file"></i> Feedback List </h1>
                    </center>
                    <div class="description">
                        <div class="container mt-5 px-2">
                            <div class="table-responsive">
                                <table id="feedbacklist" class="table table-striped table-bordered">
                                    <thead class="table-dark">
                                        <tr>
                                            <th scope="col" style="text-align:center;vertical-align: middle;">No</th>
                                            <th scope="col" style="text-align:center;vertical-align: middle;">Name</th>
                                            <th scope="col" style="text-align:center;vertical-align: middle;">Question</th>
                                            <th scope="col" style="text-align:center;vertical-align: middle;">Status</th>
                                            <th scope="col" style="text-align:center;vertical-align: middle;">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Edit Feedback -->
            <div id="EditFeedback" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form enctype='multipart/form-data' method="POST" action="" class="well form-horizontal">
                            <div class="modal-header">
                                <h5 class="modal-title">Edit Feedback</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i style="color:white" class="fas fa-times-circle"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div id="modal-container">
                                    <div class="description">
                                        <input type="hidden" name="id" id="id">
                                        <div class="form-group row">
                                            <label for="customer_name" class="col-sm-3 col-form-label">Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" autocomplete="off" name="name" id="name">
                                            </div>
                                        </div>

                                        <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                        <div class="form-group row">
                                            <label for="contact_no" class="col-sm-3 col-form-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" autocomplete="off" name="email" id="email">
                                            </div>
                                        </div>

                                        <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                        <div class="form-group row">
                                            <label for="contact_no" class="col-sm-3 col-form-label">Question</label>
                                            <div class="col-sm-9">
                                                <textarea rows="5" class="form-control" name="question" id="question"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                        <div class="form-group row">
                                            <label for="contact_no" class="col-sm-3 col-form-label">Answer</label>
                                            <div class="col-sm-9">
                                                <textarea rows="5" class="form-control" name="answer" id="answer"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <center>
                                    <div class="button-row" style="margin-right: 250px;">
                                        <div class="modal-button-save"><a id="btnEdit" title="Edit Feedback"></a></div>
                                    </div>
                                    <div class="button-row" style="margin-right: 250px;">
                                        <div class="modal-button-delete"><a id="btnDelete" title="Delete Feedback"></a></div>
                                    </div>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End Edit Customer -->
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $('#btnEdit').click(function(){
            obj = {
                id:$('#EditFeedback #id').val(),
                name:$('#EditFeedback #name').val(),
                email:$('#EditFeedback #email').val(),
                question:$('#EditFeedback #question').val(),
                answer:$('#EditFeedback #answer').val(),
            };
            
            $.ajax({
                type:'POST',
                url:'actionfeedback.php?action=edit',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'feedbacklist.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        $('#btnDelete').click(function(){
            obj = {
                id:$('#EditFeedback #id').val(),
            };
            
            $.ajax({
                type:'POST',
                url:'actionfeedback.php?action=delete',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'feedbacklist.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        $(document).on('click','.openFeedback',function(){
            var id = $(this).data('id');

            $.ajax({
                url: 'actionfeedback.php?id='+id+'&action=getFeedback',
                success: function(data){
                    $('#EditFeedback #id').val(data.id);
                    $('#EditFeedback #name').val(data.name);
                    $('#EditFeedback #email').val(data.email);
                    $('#EditFeedback #question').text(data.question);
                    $('#EditFeedback #answer').text(data.answer);
                }
            });
        });

        $('.close').click(function(){
            $('.modal').modal('hide');
        });
    </script>
</body>
</html>