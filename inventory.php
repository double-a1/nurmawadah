<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href="assets/css/inventory.css" rel="stylesheet">
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                            <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-box"></i><small> Inventory</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="d-flex flex-column justify-content-center align-items-center">
                <center>
                    <h2 class="page-title"><i class="bx bx-box"></i> Inventory </h2>
                    <div class="center">
                        <button class="fancy">
                            <span class="top-key"></span>
                            <a href="addinventory.php">Add Inventory</a>
                            <span class="bottom-key-1"></span>
                            <span class="bottom-key-2"></span>
                        </button>
                    </div>
                    <div class="container mt-2 px-5 py-5 bg-light">
                        <div class="row">
                            <?php
                                $sql = "SELECT * FROM inventory";
                                $statement = $conn->query($sql);
                                $result = $statement->fetch_all(MYSQLI_ASSOC);
                            
                                foreach($result as $row){
                                    if(intval($row['quantity']) < 3){
                                        $item_status = 'Running out of stock';
                                    }else{
                                        $item_status = '';
                                    }
                                    echo    '<div class="col-md-3 col-sm-6" style="margin-bottom: 30px;">
                                                <div class="product-grid">
                                                    <div class="product-image">
                                                        <a href="javascript:void(0)" class="image">
                                                            <img class="pic-1" style="margin-top: 20px; width:125px !important; height:150px !important" src="assets/img/inventory/'.$row["image"].'">
                                                            <img class="pic-2" style="margin-top: 20px; width:125px !important; height:150px !important; left: 20%;" src="assets/img/inventory/'.$row["image"].'">
                                                        </a>
                                                        <ul class="product-links">
                                                            <li><a href="actioninventory.php?id='.$row["id"].'&quantity='.$row["quantity"].'&action=minus_quantity"><i class="bx bx-minus"></i></a></li>
                                                            <li><a href="editinventory.php?id='.$row["id"].'"><i class="bx bx-pencil"></i></a></li>
                                                            <li><a href="actioninventory.php?id='.$row["id"].'&quantity='.$row["quantity"].'&action=plus_quantity"><i class="bx bx-plus"></i></a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="product-content" style="height:180px !important">
                                                        <div style="margin-bottom:10px" class="rating" data-rateyo-rating="'.$row["rating"].'"></div>
                                                        <h3 class="title"><a href="javascript:void(0)">'.$row["item_name"].'</a></h3>
                                                        <div class="price">RM '.$row["price"].'</div>
                                                        <div>Quantity: '.$row["quantity"].'</div>
                                                        <div style="color:red;font-weight:bold">'.$item_status.'</div>
                                                    </div>
                                                </div>
                                            </div>';
                                }
                            ?>
                        </div>
                    </div>
                </center>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $(".rating").rateYo({
            starWidth: "20px",
            readOnly: true
        });
    </script>
</body>
</html>