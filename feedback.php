<?php
    include 'connect.php';
?>
<?php include 'layout/head.php';?>
<link href="assets/css/feedback.css" rel="stylesheet">
<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<section class="breadcrumbs">
    <div class="container">
        <div class="container d-flex">
            <ul class="pagination" style="padding-left: 425px; text-align: center">
                <li class="page-item"><h4>NURMAWADAH MUSLIMAH HAIR SALOON</h4></li>
            </ul>
            <ul class="pagination" style="padding-left: 120px;">
                <li class="page-item active"><a class="page-link" href="index.php"><small> HOME</small></a></li>
                <li class="page-item active"><a class="page-link" href="login.php"><small> LOGIN</small></a></li>
            </ul>
        </div>
    </div>
</section>
<h1>Question about the salon.</h1>
<a href="addfeedback.php" style="margin-left:720px;"><button type="button" class="btn btn-dark">Ask Question</button></a>
<div class="accordion-wrapper">
    <?php
        $sql = "SELECT * FROM feedback WHERE status = 'complete'";
        $statement = $conn->query($sql);
        $result = $statement->fetch_all(MYSQLI_ASSOC);

        foreach($result as $key => $row){
            $checked = '';
            if ($key == 0) {
                $checked = 'checked';
            }
            echo    '<div class="accordion">
                        <input type="radio" name="radio-a" id="check'.$key.'" '.$checked.'>
                        <label class="accordion-label" for="check'.$key.'">'.$row['question'].'</label>
                        <div class="accordion-content">
                            <p>'.$row['answer'].'</p>
                        </div>
                    </div>';
        }
    ?>
</div>