<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href="assets/css/service.css" rel="stylesheet">
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                            <li class="page-item active"><a class="page-link" href="#"><i class="bx bxs-shopping-bag"></i><small> Service</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="d-flex flex-column justify-content-center align-items-center">
                <center>
                    <h2 class="page-title"><i class="bx bxs-shopping-bag"></i> Service </h2>
                    <div class="center">
                        <button class="fancy">
                            <span class="top-key"></span>
                            <a href="addservice.php" class="">Add Service</a>
                            <span class="bottom-key-1"></span>
                            <span class="bottom-key-2"></span>
                        </button>
                    </div>
                    <div class="container mt-5">
                        <?php
                            $sql = "SELECT * FROM service";
                            $statement = $conn->query($sql);
                            $result = $statement->fetch_all(MYSQLI_ASSOC);
                        
                            foreach($result as $row){
                                $price = explode('.',$row['price']);
                                $description = explode(',',$row['description']);
                                
                                $html = '';
                                foreach($description as $desc){
                                    $html .= '<li>'.$desc.'</li>';
                                }

                                echo    '<div class="whole p-1">
                                            <div class="type" style="background-color:'.$row["color"].';border-bottom: 3px solid '.$row["color"].';">
                                                <p>'.$row["service_type"].'</p>
                                            </div>
                                            <div class="plan">
                                                <div class="header-service">
                                                    <span>RM</span>'.$price[0].'<sup>'.$price[1].'</sup>
                                                    <p class="month">per person</p>
                                                </div>
                                                <div class="content">
                                                    <ul>
                                                        '.$html.'
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <button class="fancy" style="float:none">
                                                        <span class="top-key"></span>
                                                        <a href="editservice.php?id='.$row["id"].'" class="">Edit Service</a>
                                                        <span class="bottom-key-1"></span>
                                                        <span class="bottom-key-2"></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>';
                            }
                        ?>
                    </div>
                </center>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
</body>
</html>