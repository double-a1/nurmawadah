<?php
    session_start();
    include 'connect.php';
    include 'session.php';

    $id = $_GET["id"];

    $sql = "SELECT * FROM user WHERE id='$id'";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($result);

    $username = $row["username"];
    $name = $row["name"];
    $contact_no = $row["contact_no"];
    $image = $row["image"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                <div class="container d-flex">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                        <li class="page-item"><a class="page-link" href="profile.php"><i class="bx bx-user"></i><small> My Profile</small></a></li>
                        <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-pencil"></i><small> Edit Profile</small></a></li>
                    </ul>
                </div>
                </div>
            </section>
            <section class="d-flex flex-column justify-content-center align-items-center">
                <h2 class="page-title"><i class="bx bx-user"></i> Edit Profile </h2>
                <div class="container">
                    <div class="card">
                        <div class="card-body">
                            <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
                            <div class="form-group row">
                                <label for="staff_id" class="col-sm-2 col-form-label">Staff ID</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="staff_id" id="staff_id" value="<?php echo $staff_id ?>">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="name" id="name" value="<?php echo $name ?>">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="contact_no" class="col-sm-2 col-form-label">Contact No</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" autocomplete="off" name="contact_no" id="contact_no" value="<?php echo $contact_no ?>">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="password" class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" autocomplete="off" name="password" id="password">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <div class="form-group row">
                                <label for="confirm_password" class="col-sm-2 col-form-label">Confirm Password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" autocomplete="off" name="confirm_password" id="confirm_password">
                                </div>
                            </div>

                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                            <?php if($image != null){ ?>
                                <div class="form-group row">
                                    <label for="image" class="col-sm-2 col-form-label">Image</label>
                                    <div class="col-sm-10">
                                        <div class="col-md-3" style="text-align:center;border: 2px solid">
                                            <img id="image_preview" style="margin-top: 20px; width:125px !important; height:150px !important" src="assets/img/profile/<?php echo $image ?>">
                                            <div class="btn-group" style="display:flex;" role="group">
                                                <button type="button" style="border-top:2px solid;border-radius: 0;" class="btn btn-outline-dark edit-image"><i class="bx bx-pencil"></i></button>
                                                <button type="button" style="border-top:2px solid;border-radius: 0; display:none" class="btn btn-outline-dark upload-image"><i class="bx bx-upload"></i></button>
                                                <input type="file" style="display:none" class="form-control" onchange="readURL(this)" name="edit_image" id="edit_image">
                                                <input type="hidden" name="old_image" id="old_image" value="<?php echo $image ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php }else{ ?>
                                <div class="form-group row">
                                    <label for="image" class="col-sm-2 col-form-label">Image</label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" autocomplete="off" name="image" id="image">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="card-footer">
                            <center>
                                <div class="button-row" style="margin-right: 300px;">
                                    <div><a id="btnEdit" title="Edit Profile"></a></div>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $('.edit-image').click(function(){
            $('#edit_image').trigger('click');
        });

        $('.upload-image').click(function(){
            var id = $('#id').val();

            var formData = new FormData();
            formData.append('id',$('#id').val());
            formData.append('old_image',$('#old_image').val());
            formData.append('edit_image',$('#edit_image').prop('files')[0]);

            $.ajax({
                type:'POST',
                url:'actionprofile.php?action=uploadimage',
                data:formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'editprofile.php?id='+id+''
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                $('.edit-image').hide();
                $('.upload-image').show();
            }
        }
        
        $('#btnEdit').click(function(){
            obj = {
                id:$('#id').val(),
                username:$('#username').val(),
                name:$('#name').val(),
                contact_no:$('#contact_no').val(),
                password:$('#password').val(),
                confirm_password:$('#confirm_password').val(),
            };
            
            $.ajax({
                type:'POST',
                url:'actionprofile.php?action=edit',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'profile.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });
    </script>
</body>
</html>