DROP DATABASE IF EXISTS `nurmawadah`;
CREATE DATABASE IF NOT EXISTS `nurmawadah`;
USE `nurmawadah`;

DROP TABLE IF EXISTS `booking`;
CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_name` text NOT NULL,
  `contact_no` text NOT NULL,
  `title` text NOT NULL,
  `service_type` text NOT NULL,
  `remarks` text NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `status` text NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `service`;
CREATE TABLE IF NOT EXISTS `service` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `service_type` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `price` text NOT NULL,
  `color` text NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `service` (`service_type`, `title`, `description`, `price`, `color`, `updated_date`) VALUES
('Package A', 'Hair Cut', 'Hair Cut', '20.00', '#2B65EC', CURRENT_TIMESTAMP),
('Package B', 'Wash, Massage & Blow', 'Wash,Massage,Blow', '50.00', '#00B300', CURRENT_TIMESTAMP),
('Package C', 'Henna Color', 'Henna Color', '35.00', '#EDB32B', CURRENT_TIMESTAMP);

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_name` text NOT NULL,
  `staff_name` text NOT NULL,
  `service_type` text NOT NULL,
  `price` text NOT NULL,
  `total_price` text NOT NULL,
  `paid` text NOT NULL,
  `balance` text NOT NULL,
  `status` text NOT NULL,
  `date` date NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `inventory`;
CREATE TABLE IF NOT EXISTS `inventory` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_name` text NOT NULL,
  `description` text NOT NULL,
  `quantity` text NOT NULL,
  `rating` text NOT NULL,
  `price` text NOT NULL,
  `image` text NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `inventory` (`item_name`, `description`, `quantity`, `rating`, `price`, `image`, `updated_date`) VALUES
('Head and Shoulders Royal Oils Moisture Boost Shampoo', 'For washing hair', '5', '0', '25.00', 'shampoo.jpg',CURRENT_TIMESTAMP),
('Dove Hair Fall Rescue Conditioner', 'For shining hair', '5', '0', '45.00', 'conditioner.jpg',CURRENT_TIMESTAMP),
('Soft Touch Black Henna', 'For hair coloring', '5', '0', '65.00', 'henna.jpg',CURRENT_TIMESTAMP),
('Sunsilk Natural Hair Serum Coconut', 'For hair treatment', '5', '0', '100.00', 'serum.jpg',CURRENT_TIMESTAMP),
('Ivy Silkshine Colour & Damage Care Hair Mask', 'Repair hair cell', '5', '0', '150.00', 'hair_mask.jpg',CURRENT_TIMESTAMP);

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_name` text NOT NULL,
  `contact_no` text NOT NULL,
  `total_visit` text NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `cron`;
CREATE TABLE IF NOT EXISTS `cron` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` text NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `attendance`;
CREATE TABLE IF NOT EXISTS `attendance` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `user_type` text NOT NULL,
  `checkin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `checkout` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `checkin_status` text NOT NULL,
  `checkout_status` text NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `attendance` (`username`, `user_type`, `checkin_status`, `checkout_status`, `updated_date`) VALUES
('admin', 'admin', '0', '0', CURRENT_TIMESTAMP),
('staff', 'staff', '0', '0', CURRENT_TIMESTAMP);

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `name` text NOT NULL,
  `password` text NOT NULL,
  `contact_no` text NOT NULL,
  `user_type` text NOT NULL,
  `image` text NULL,
  `booking_status` VARCHAR(255) NOT NULL DEFAULT '0',
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `user` (`username`, `name`, `password`, `contact_no`, `user_type`, `image`, `updated_date`) VALUES
('admin', 'Administrator', MD5('123'), '0123456789', 'admin', 'profile-img1.jpg', CURRENT_TIMESTAMP),
('staff', 'Staff', MD5('123'), '0123456789', 'staff', 'profile-img2.jpg', CURRENT_TIMESTAMP),
('member', 'Member', MD5('123'), '0123456789', 'member', 'profile-img3.jpg', CURRENT_TIMESTAMP);

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `inventory_id` int(10) NOT NULL,
  `customer_name` text NOT NULL,
  `item_name` text NOT NULL,
  `quantity` text NOT NULL,
  `price` text NOT NULL,
  `total_price` text NOT NULL,
  `status` text NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_id` text NOT NULL,
  `customer_name` text NOT NULL,
  `address` text NOT NULL,
  `payment_method` text NOT NULL,
  `total_price` text NOT NULL,
  `status` text NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `question` text NOT NULL,
  `answer` text NULL,
  `status` text NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;