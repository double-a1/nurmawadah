<?php
	header('Content-Type: application/json');

	include 'connect.php';

	$action = ( isset($_GET["action"]) && !empty($_GET["action"]) ) ? $_GET["action"] : 'table';

	switch($action) {
		case 'edit':
			$id = $_POST['id'];
			$staff_name  = $_POST['staff_name'];
			$service_type  = $_POST['service_type'];
			$price = $_POST['price'];
			$total_price = $_POST['total_price'];
			$paid = $_POST['paid'];
			$balance = $_POST['balance'];

			$query = "UPDATE transaction SET 
				staff_name = '$staff_name', 
				service_type = '$service_type', 
				price = '$price', 
				total_price = '$total_price',
				paid = '$paid',
				balance = '$balance',
				updated_date = CURRENT_TIMESTAMP
				WHERE id = '$id'";
			
			if(mysqli_query($conn, $query)){
				$msg = "Transaction edited successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'submit':
			$id = $_POST['id'];
			$staff_name  = $_POST['staff_name'];
			$service_type  = $_POST['service_type'];
			$price = $_POST['price'];
			$total_price = $_POST['total_price'];
			$paid = $_POST['paid'];
			$balance = $_POST['balance'];
			$status = 'complete';

			$query = "UPDATE transaction SET 
				staff_name = '$staff_name', 
				service_type = '$service_type', 
				price = '$price', 
				total_price = '$total_price',
				paid = '$paid',
				balance = '$balance',
				status = '$status',
				updated_date = CURRENT_TIMESTAMP
				WHERE id = '$id'";
			
			if(mysqli_query($conn, $query)){
				$msg = "Transaction submitted successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'getTransaction':
			$id = $_GET["id"];

			$sql = "SELECT * FROM transaction WHERE id='$id'";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_array($result);

			$data = array(
				'id'            => $row["id"],
				'customer_name' => $row["customer_name"],
				'staff_name' 	=> $row["staff_name"],
				'service_type'  => $row["service_type"],
				'price'         => $row["price"],
				'total_price' 	=> $row["total_price"],
				'paid' 			=> $row["paid"],
				'balance' 		=> $row["balance"]
			);

			echo json_encode($data);
		break;

		case 'getPrice':
			$service_type = $_GET["service_type"];
			$service = join("','",$service_type);
			$service = "'".$service."'";

			$sql = "SELECT * FROM service WHERE service_type IN ({$service})";
			$statement = $conn->query($sql);
			$result = $statement->fetch_all(MYSQLI_ASSOC);

			$price = '';
			$total_price = 0;
			foreach($result as $key => $row){
				if($key+1 < count($result)){
					$price .= 'RM '.$row["price"].' + ';
				}else{
					$price .= 'RM '.$row["price"];
				}
				$total_price = $total_price + floatval($row["price"]);
			}

			$data = array(
				'price'         => $price,
				'total_price' 	=> $total_price
			);

			echo json_encode($data);
		break;

		default:
			$sql = "SELECT * FROM transaction ORDER BY id DESC";
			$statement = $conn->query($sql);
			$result = $statement->fetch_all(MYSQLI_ASSOC);
		
			$data = [];
			foreach($result as $key => $row){
				$staff_name = $row["staff_name"];
				$sql1 = "SELECT * FROM user WHERE name='$staff_name'";
				$result1 = mysqli_query($conn, $sql1);
				$row1 = mysqli_fetch_array($result1);

				if($result1 != null){
					$data[] = array(
						'key'           => $key+1,
						'id'            => $row["id"],
						'customer_name' => $row["customer_name"],
						'staff_name' 	=> $row["staff_name"],
						'image'   		=> $row1["image"] ?? " - ",
						'service_type'  => $row["service_type"],
						'price'         => $row["price"],
						'total_price' 	=> $row["total_price"],
						'status'	 	=> $row["status"],
						'date'   		=> $row["date"],
					);
				}else{
					$data[] = array(
						'key'           => $key+1,
						'id'            => $row["id"],
						'customer_name' => $row["customer_name"],
						'staff_name' 	=> $row["staff_name"],
						'image'   		=> '',
						'service_type'  => $row["service_type"],
						'price'         => $row["price"],
						'total_price' 	=> $row["total_price"],
						'status'	 	=> $row["status"],
						'date'   		=> $row["date"],
					);
				}
			}
			echo json_encode($data);
		break;
	}
 ?>