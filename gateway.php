<?php
    session_start();
    include 'connect.php';
    include 'session.php';

    $total_price = $_SESSION['total_price'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link href="assets/css/gateway.css" rel="stylesheet">
</head>
<body>
    <form>
        <div class="form-container">
        <div class="personal-information">
            <h1>Make Order Payment</h1>
        </div> <!-- end of personal-information -->
        <input id="column-left" type="text" name="first-name" placeholder="First Name"/>
        <input id="column-right" type="text" name="last-name" placeholder="Surname"/>
        <input id="input-field" type="text" name="number" placeholder="Card Number"/>
        <input id="column-left" type="text" name="expiry" placeholder="MM / YY"/>
        <input id="column-right" type="text" name="cvc" placeholder="CCV"/>
        <div class="card-wrapper"></div>
        <div class="checkout" style="margin-left: 220px;">
            <div><a id="btnAdd" title="Pay"></a></div>
        </div>
    </form>

    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/card-master/dist/jquery.card.js"></script>
    <script src="assets/vendor/sweetalert/main.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
        $('form').card({
            container: '.card-wrapper',
            width: 280,

            formSelectors: {
                nameInput: 'input[name="first-name"], input[name="last-name"]'
            }
        });
        $('#btnAdd').click(function(){
            var url_string = document.URL;
            var url = new URL(url_string);
            var obj = url.searchParams.get("data");

            obj = JSON.parse(atob(obj));
            $.ajax({
                type:'POST',
                url:'actionpayment.php',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            window.open('receipt_payment.php?id='+data.id);
                        }).finally(()=>{
                            location.href = 'product.php';
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });
    </script>
</body>
</html>