<?php
	header('Content-Type: application/json');

	include 'connect.php';

	$action = ( isset($_GET["action"]) && !empty($_GET["action"]) ) ? $_GET["action"] : 'table';

	switch($action) {
        case 'add' :
			$customer_name = $_POST["customer_name"];
			$contact_no = $_POST["contact_no"];
			$total_visit = 0;

			$query = "INSERT INTO customer (
				customer_name, 
				contact_no, 
				total_visit,
				updated_date)
			VALUES (
				'$customer_name',
				'$contact_no',
				'$total_visit',
				CURRENT_TIMESTAMP)";
			
			if(mysqli_query($conn, $query)){
				$msg = "Customer added successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'edit':
			$id = $_POST['id'];
			$customer_name = $_POST["customer_name"];
			$contact_no = $_POST["contact_no"];

			$query = "UPDATE customer SET 
				customer_name = '$customer_name',
				contact_no = '$contact_no',
				updated_date = CURRENT_TIMESTAMP
				WHERE id = '$id'";
			
			if(mysqli_query($conn, $query)){
				$msg = "Customer edited successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'delete':
			$id = $_POST['id'];
			
			$query = "DELETE FROM customer WHERE id = '$id'";
			if(mysqli_query($conn, $query)){
				$msg = "Customer deleted successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}
			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'checkUser':
			$customer_name = $_POST["customer_name"];

			$sql = "SELECT * FROM customer WHERE customer_name='$customer_name'";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_array($result);

			echo json_encode($row["contact_no"]);
		break;

		case 'getCustomer':
			$id = $_GET["id"];

			$sql = "SELECT * FROM customer WHERE id='$id'";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_array($result);

			$data = array(
				'id'            => $row["id"],
				'customer_name' => $row["customer_name"],
				'contact_no' 	=> $row["contact_no"]
			);

			echo json_encode($data);
		break;

		default:
			$sql = "SELECT * FROM customer";
			$statement = $conn->query($sql);
			$result = $statement->fetch_all(MYSQLI_ASSOC);
			
			$data = [];
			foreach($result as $key => $row){
				$data[] = array(
                    'key'           => $key+1,
					'id'            => $row["id"],
					'customer_name' => $row["customer_name"],
					'contact_no' 	=> $row["contact_no"],
					'total_visit'  => $row["total_visit"],
				);
			}
			echo json_encode($data);
		break;
	}
 ?>