<?php
	header('Content-Type: application/json');

	session_start();
	include 'connect.php';
	include 'session.php';

	$action = $_GET["action"];

	$name = $_SESSION['name'];

	switch($action) {
		case 'add' :
			$customer_name = $name;
			$item_id = $_GET["id"];
			$item_name = $_GET["item_name"];
			$price = $_GET["price"];
			$quantity = 1;
			$status = 'progress';

			$sql = "SELECT quantity, price, total_price FROM cart WHERE customer_name='$customer_name' AND inventory_id='$item_id' AND status='progress'";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_array($result);

			if (!$row) {
				$query = "INSERT INTO cart (
					inventory_id,
					customer_name, 
					item_name, 
					quantity,
					price,
					total_price,
					status,
					updated_date)
				VALUES (
					'$item_id',
					'$customer_name',
					'$item_name',
					'$quantity',
					'$price',
					'$price',
					'$status',
					CURRENT_TIMESTAMP)";
			} else {
				$total_price = intval($row['total_price']) + intval($price);
				$quantity = intval($row['quantity']) + intval($quantity);
				$query = "UPDATE cart SET 
				total_price = '$total_price',
				quantity = '$quantity',
				updated_date = CURRENT_TIMESTAMP
				WHERE inventory_id = '$item_id' AND status = 'progress'";
			}
			
			if(mysqli_query($conn, $query)){
				$sql = "SELECT quantity FROM inventory WHERE id='$item_id'";
				$result = mysqli_query($conn, $sql);
				$row = mysqli_fetch_array($result);
				$total_quantity = intval($row['quantity']) - 1;
				
				$sql = "UPDATE inventory 
						SET quantity='$total_quantity'
						WHERE id='$item_id'";
				mysqli_query($conn, $sql);
				header("location:product.php");
			}
		break;

        case 'deleteCart':
			$customer_name = $name;
			$item_id = $_GET["id"];
			$price = $_GET["price"];
			$quantity = 1;
			$status = 'progress';

			$sql = "SELECT quantity, price, total_price FROM cart WHERE customer_name='$customer_name' AND inventory_id='$item_id' AND status='progress'";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_array($result);

			if ($row['quantity'] == '1') {
				$query = "DELETE FROM cart WHERE inventory_id='$item_id' AND customer_name='$customer_name' AND status='progress'";
			} else {
				$total_price = intval($row['total_price']) - intval($price);
				$quantity = intval($row['quantity']) - intval($quantity);
				$query = "UPDATE cart SET 
				total_price = '$total_price',
				quantity = '$quantity',
				updated_date = CURRENT_TIMESTAMP
				WHERE inventory_id = '$item_id' AND status = 'progress'";
			}
			
			if(mysqli_query($conn, $query)){
				$sql = "SELECT quantity FROM inventory WHERE id='$item_id'";
				$result = mysqli_query($conn, $sql);
				$row = mysqli_fetch_array($result);
				$total_quantity = intval($row['quantity']) + 1;
				
				$sql = "UPDATE inventory 
						SET quantity='$total_quantity'
						WHERE id='$item_id'";
				mysqli_query($conn, $sql);
				header("location:product.php");
			}
		break;

		case 'deleteCheckout':
			$customer_name = $name;
			$item_id = $_GET["id"];
			$price = $_GET["price"];
			$quantity = 1;
			$status = 'progress';

			$sql = "SELECT quantity, price, total_price FROM cart WHERE customer_name='$customer_name' AND inventory_id='$item_id' AND status='progress'";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_array($result);

			if ($row['quantity'] == '1') {
				$query = "DELETE FROM cart WHERE inventory_id='$item_id' AND customer_name='$customer_name' AND status='progress'";
			} else {
				$total_price = intval($row['total_price']) - intval($price);
				$quantity = intval($row['quantity']) - intval($quantity);
				$query = "UPDATE cart SET 
				total_price = '$total_price',
				quantity = '$quantity',
				updated_date = CURRENT_TIMESTAMP
				WHERE inventory_id = '$item_id' AND status = 'progress'";
			}
			
			if(mysqli_query($conn, $query)){
				$sql = "SELECT quantity FROM inventory WHERE id='$item_id'";
				$result = mysqli_query($conn, $sql);
				$row = mysqli_fetch_array($result);
				$total_quantity = intval($row['quantity']) + 1;
				
				$sql = "UPDATE inventory 
						SET quantity='$total_quantity'
						WHERE id='$item_id'";
				mysqli_query($conn, $sql);
				header("location:addproduct.php");
			}
		break;

		default:
		
		break;
	}
 ?>