<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href="assets/css/transaction.css" rel="stylesheet">
    <link href="assets/css/customerlist.css" rel="stylesheet">
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
            <div class="container">
                <div class="container d-flex">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                    <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-id-card"></i><small> Staff List</small></a></li>
                </ul>
                </div>
            </div>
            </section>
            <section class="d-flex flex-column justify-content-center align-items-center">
                <center>
                    <h2 class="page-title"><i class="bx bx-id-card"></i> Staff List </h2>
                    <div class="center">
                        <button class="fancy">
                            <span class="top-key"></span>
                            <a href="#" data-bs-toggle="modal" data-bs-target="#AddStaff">Add Staff</a>
                            <span class="bottom-key-1"></span>
                            <span class="bottom-key-2"></span>
                        </button>
                    </div>
                </center>
                <div class="container mt-5 px-2">
                    <div class="table-responsive">
                        <table id="stafflist" class="table table-striped table-bordered">
                            <thead class="table-dark">
                                <tr>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">No</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Staff ID</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Name</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Contact No</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">User Type</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <!-- Add Staff -->
                <div id="AddStaff" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form enctype='multipart/form-data' method="POST" action="" class="well form-horizontal">
                                <div class="modal-header">
                                    <h5 class="modal-title">Add Staff</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i style="color:white" class="fas fa-times-circle"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="staff_id" class="col-sm-3 col-form-label">Staff ID</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" autocomplete="off" name="staff_id" id="staff_id">
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-3 col-form-label">Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" autocomplete="off" name="name" id="name">
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="contact_no" class="col-sm-3 col-form-label">Contact No</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" autocomplete="off" name="contact_no" id="contact_no">
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="user_type" class="col-sm-3 col-form-label">User Type</label>
                                                <div class="col-sm-9">
                                                    <select class="form-select" name="user_type" id="user_type">
                                                        <option value="">Select user type</option>
                                                        <option value="admin">Admin</option>
                                                        <option value="staff">Staff</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <div class="button-row" style="margin-right: 400px;">
                                            <div class="modal-button-save"><a id="btnAdd" title="Add Staff"></a></div>
                                        </div>
                                    </center>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Add Staff -->

                <!-- Edit Staff -->
                <div id="EditStaff" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form enctype='multipart/form-data' method="POST" action="" class="well form-horizontal">
                                <div class="modal-header">
                                    <h5 class="modal-title">Edit Staff</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i style="color:white" class="fas fa-times-circle"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="card">
                                        <div class="card-body">
                                            <input type="hidden" name="id" id="id">
                                            <div class="form-group row">
                                                <label for="staff_id" class="col-sm-3 col-form-label">Staff ID</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" autocomplete="off" name="staff_id" id="staff_id">
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-3 col-form-label">Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" autocomplete="off" name="name" id="name">
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="contact_no" class="col-sm-3 col-form-label">Contact No</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" autocomplete="off" name="contact_no" id="contact_no">
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="user_type" class="col-sm-3 col-form-label">User Type</label>
                                                <div class="col-sm-9">
                                                    <select class="form-select" name="user_type" id="user_type">
                                                        <option value="admin">Admin</option>
                                                        <option value="staff">Staff</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <div class="button-row" style="margin-right: 250px;">
                                            <div class="modal-button-save"><a id="btnEdit" title="Edit Staff"></a></div>
                                        </div>
                                        <div class="button-row" style="margin-right: 250px;">
                                            <div class="modal-button-delete"><a id="btnDelete" title="Delete Staff"></a></div>
                                        </div>
                                    </center>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Edit Staff -->
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $('#btnAdd').click(function(){
            obj = {
                username:$('#AddStaff #username').val(),
                name:$('#AddStaff #name').val(),
                contact_no:$('#AddStaff #contact_no').val(),
                user_type:$('#AddStaff #user_type').val()
            };
            
            $.ajax({
                type:'POST',
                url:'actionstafflist.php?action=add',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'stafflist.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        $('#btnEdit').click(function(){
            obj = {
                id:$('#EditStaff #id').val(),
                username:$('#EditStaff #username').val(),
                name:$('#EditStaff #name').val(),
                contact_no:$('#EditStaff #contact_no').val(),
                user_type:$('#EditStaff #user_type').val(),
            };
            
            $.ajax({
                type:'POST',
                url:'actionstafflist.php?action=edit',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'stafflist.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        $('#btnDelete').click(function(){
            obj = {
                id:$('#EditStaff #id').val(),
            };
            
            $.ajax({
                type:'POST',
                url:'actionstafflist.php?action=delete',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'stafflist.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        $(document).on('click','.openStaff',function(){
            var id = $(this).data('id');

            $.ajax({
                url: 'actionstafflist.php?id='+id+'&action=getStaff',
                success: function(data){
                    $('#EditStaff #id').val(data.id);
                    $('#EditStaff #username').val(data.username);
                    $('#EditStaff #name').val(data.name);
                    $('#EditStaff #contact_no').val(data.contact_no);
                    $('#EditStaff #user_type').val(data.user_type);
                }
            });
        });

        $('.close').click(function(){
            $('.modal').modal('hide');
        });
    </script>
</body>
</html>