<footer id="footer">
    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span> Programmer </span></strong>
        </div>
    </div>
</footer>

<!-- Vendor JS Files -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/datatable/datatables.min.js" type="text/javascript"></script>
<script src="assets/vendor/fullcalendar/js/main.min.js"></script>
<script src="assets/vendor/sweetalert/main.min.js"></script>
<script src="assets/vendor/aos/aos.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>
<script src="assets/vendor/purecounter/purecounter.js"></script>
<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="assets/vendor/typed.js/typed.min.js"></script>
<script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
<script src="assets/vendor/calculator/js/calculate.js"></script>
<script src="assets/vendor/rateyo/jquery.rateyo.min.js"></script>
<script src="assets/vendor/snap/snap.js"></script>
<script src="assets/vendor/plotly/plotly-2.16.1.min.js"></script>
<script src="assets/card-master/dist/jquery.card.js"></script>
<script src="assets/js/main.js"></script>