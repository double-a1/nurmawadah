<style>
    .badge-menu {
        border-radius: 10px;
        margin-left: 5px;
        padding: 2px 6px;
        font-weight: bold;
        color: red;
        background: white;
    }
</style>

<i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
<header id="header">
    <div class="d-flex flex-column">
        <div class="profile">
            <?php if(empty($profile_image)){ ?>
                <img src="assets/img/avatar.png " class="img-fluid rounded-circle">
            <?php }else{ ?>
                <img src="assets/img/profile/<?php echo $profile_image ?>" class="img-fluid rounded-circle">
            <?php }?>
            <h1 class="text-light"><a href="index.html"><?php echo $name ?></a></h1>
            <div class="social-links mt-3 text-center">
                <a href="profile.php" class="nav-link"><i class="bx bx-user"></i></a>
                <h5 class="mt-2 text-light"><p>My Profile</p></h5>
            </div>
        </div>

        <nav id="navbar" class="nav-menu navbar">
            <ul>
                <?php if($user_type == 'admin'){ ?>
                    <li><a href="home.php" class="nav-link"><i class="bx bx-home"></i> <span>Home</span></a></li>
                    <li><a href="dashboard.php" class="nav-link"><i class="bx bx-chart"></i> <span>Dashboard</span></a></li>
                    <li><a href="attendance_admin.php" class="nav-link"><i class="bx bx-time"></i> <span>Attendance List</span></a></li>
                    <li><a href="booking.php" class="nav-link"><i class="bx bx-calendar"></i> <span>Booking</span></a></li>
                    <?php if($booking == '0'){ ?>
                        <li><a href="booking_detail.php" class="nav-link"><i class="bx bx-table"></i> <span>Booking Detail </span></a></li>
                    <?php }else{ ?>
                        <li><a href="booking_detail.php" class="nav-link"><i class="bx bx-table"></i> <span>Booking Detail <span class="badge-menu"><?php echo $booking; ?></span></span></a></li>
                    <?php }?>
                    <li><a href="transaction.php" class="nav-link"><i class="bx bx-calculator"></i> <span>Transaction</span></a></li>
                    <li><a href="service.php" class="nav-link"><i class="bx bxs-shopping-bag"></i> <span>Service</span></a></li>
                    <li><a href="inventory.php" class="nav-link"><i class="bx bx-box"></i> <span>Inventory</span></a></li>
                    <li><a href="customerlist.php" class="nav-link"><i class="bx bx-group"></i> <span>Customer List</span></a></li>
                    <li><a href="stafflist.php" class="nav-link"><i class="bx bx-id-card"></i> <span>Staff List</span></a></li>
                    <li><a href="feedbacklist.php" class="nav-link"><i class="bx bx-file"></i> <span>Feedback List</span></a></li>
                    <li><a href="logout.php" class="nav-link"><i class="bx bx-log-out"></i> <span>Logout</span></a></li>
                <?php }else if($user_type == 'staff'){ ?>
                    <li><a href="home.php" class="nav-link"><i class="bx bx-home"></i> <span>Home</span></a></li>
                    <li><a href="attendance_staff.php" class="nav-link"><i class="bx bx-time"></i> <span>Attendance</span></a></li>
                    <li><a href="booking.php" class="nav-link"><i class="bx bx-calendar"></i> <span>Booking</span></a></li>
                    <?php if($booking == '0'){ ?>
                        <li><a href="booking_detail.php" class="nav-link"><i class="bx bx-table"></i> <span>Booking Detail </span></a></li>
                    <?php }else{ ?>
                        <li><a href="booking_detail.php" class="nav-link"><i class="bx bx-table"></i> <span>Booking Detail <span class="badge-menu"><?php echo $booking; ?></span></span></a></li>
                    <?php }?>
                    <li><a href="transaction.php" class="nav-link"><i class="bx bx-calculator"></i> <span>Transaction</span></a></li>
                    <li><a href="inventory.php" class="nav-link"><i class="bx bx-box"></i> <span>Inventory</span></a></li>
                    <li><a href="customerlist.php" class="nav-link"><i class="bx bx-group"></i> <span>Customer List</span></a></li>
                    <li><a href="feedbacklist.php" class="nav-link"><i class="bx bx-file"></i> <span>Feedback List</span></a></li>
                    <li><a href="logout.php" class="nav-link"><i class="bx bx-log-out"></i> <span>Logout</span></a></li>
                <?php }else{ ?>
                    <li><a href="home.php" class="nav-link"><i class="bx bx-home"></i> <span>Home</span></a></li>
                    <li><a href="booking_member.php" class="nav-link"><i class="bx bx-calendar"></i> <span>Booking </span></a></li>
                    <?php if($booking == '0'){ ?>
                        <li><a href="history.php" class="nav-link"><i class="bx bx-history"></i> <span>Booking History </span></a></li>
                    <?php }else{ ?>
                        <li><a href="history.php" class="nav-link"><i class="bx bx-history"></i> <span>Booking History <span class="badge-menu"><?php echo $booking; ?></span></span></a></li>
                    <?php }?>
                    <li><a href="product.php" class="nav-link"><i class="bx bx-box"></i> <span>Product</span></a></li>
                    <li><a href="logout.php" class="nav-link"><i class="bx bx-log-out"></i> <span>Logout</span></a></li>
                <?php } ?>
            </ul>
        </nav>
    </div>
</header>