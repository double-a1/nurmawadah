<?php
	header('Content-Type: application/json');

	include 'connect.php';

	$action = $_GET["action"];

	switch($action) {
		case 'edit':
            $id = $_POST["id"];
			$username = $_POST["username"];
			$name = $_POST["name"];
			$contact_no = $_POST["contact_no"];
			$password = $_POST["password"];
			$confirm_password = $_POST["confirm_password"];
			
            if(empty($password) && empty($confirm_password)){
                $query = "UPDATE user SET 
                    username = '$username', 
                    name = '$name', 
                    contact_no = '$contact_no',
                    updated_date = CURRENT_TIMESTAMP
                    WHERE id = '$id'";

                if(mysqli_query($conn, $query)){
                    $msg = "Profile edited successfully!";
                    $status = true;
                }else{
                    $msg = mysqli_error($conn);
                    $status = false;
                }
            }else{
                if($password != $confirm_password){
                    $msg = "Password Not Match!";
                    $status = false;
                }else{
                    $pass = md5($password);

                    $query = "UPDATE user SET 
                        username = '$username', 
                        name = '$name', 
                        contact_no = '$contact_no',
                        password = '$pass',
                        updated_date = CURRENT_TIMESTAMP
                        WHERE id = '$id'";

                    if(mysqli_query($conn, $query)){
                        $msg = "Profile edited successfully!";
                        $status = true;
                    }else{
                        $msg = mysqli_error($conn);
                        $status = false;
                    }
                }
            }

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

        case 'uploadimage' :
            $id = $_POST["id"];
            $image = $_FILES['edit_image']['name'];

            if ( 0 < $_FILES['edit_image']['error'] ) {
                $msg = $_FILES['edit_image']['error'];
            } else {
                unlink('assets/img/profile/'.$_POST['old_image']);
                $extension = pathinfo($image, PATHINFO_EXTENSION);
				$file_name = rand().'.'.$extension;
                move_uploaded_file($_FILES['edit_image']['tmp_name'], 'assets/img/profile/' . $file_name);
            }

			$query = "UPDATE user SET
                image = '$file_name',
                updated_date = CURRENT_TIMESTAMP
                WHERE id = '$id'";
			
			if(mysqli_query($conn, $query)){
				$msg = "File uploaded successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		default:
		break;
	}
 ?>