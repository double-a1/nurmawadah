<?php
	header('Content-Type: application/json');

	include 'connect.php';

	$action = ( isset($_GET["action"]) && !empty($_GET["action"]) ) ? $_GET["action"] : 'table';

	switch($action) {
		case 'add' :
			$name = $_POST["name"];
			$email = $_POST["email"];
			$question = $_POST["question"];
			$status = 'progress';

			$query = "INSERT INTO feedback (
				name, 
				email, 
				question,
				status, 
				updated_date)
			VALUES (
				'$name', 
				'$email', 
				'$question', 
				'$status', 
				CURRENT_TIMESTAMP)";
			
			if(mysqli_query($conn, $query)){
				$msg = "Question added successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'edit':
			$id = $_POST['id'];
			$name = $_POST["name"];
			$email = $_POST["email"];
			$question = $_POST["question"];
			$answer = $_POST["answer"];
			$status = $_POST["answer"] ? 'complete' : 'progress';
			
			$query = "UPDATE feedback SET 
				name = '$name', 
				email = '$email', 
				question = '$question', 
				answer = '$answer', 
				status = '$status',
				updated_date = CURRENT_TIMESTAMP
				WHERE id = '$id'";
			
			if(mysqli_query($conn, $query)){
				$msg = "Feedback edited successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}

			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'delete':
			$id = $_POST['id'];
			
			$query = "DELETE FROM feedback WHERE id = '$id'";
			if(mysqli_query($conn, $query)){
				$msg = "Feedback deleted successfully!";
				$status = true;
			}else{
				$msg = mysqli_error($conn);
				$status = false;
			}
			$data = array("status"=>$status, "message"=>$msg);
			echo json_encode($data);
		break;

		case 'getFeedback':
			$id = $_GET["id"];

			$sql = "SELECT * FROM feedback WHERE id='$id'";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_array($result);

			$data = array(
				'id'       => $row["id"],
				'name'     => $row["name"],
				'email'    => $row["email"],
				'question' => $row["question"],
				'answer'   => $row["answer"],
			);

			echo json_encode($data);
		break;

		default:
			$sql = "SELECT * FROM feedback";
			$statement = $conn->query($sql);
			$result = $statement->fetch_all(MYSQLI_ASSOC);
			
			$data = [];
			foreach($result as $key => $row){
				$data[] = array(
                    'key'     => $key+1,
					'id'      => $row["id"],
					'name' 	  => $row["name"],
					'question'=> $row["question"],
					'status'  => $row["status"],
				);
			}
			echo json_encode($data);
		break;
	}
 ?>