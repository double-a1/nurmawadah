<?php
    session_start();
    include 'connect.php';
    include 'session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'layout/head.php';?>
    <link href="assets/css/transaction.css" rel="stylesheet">
</head>
<body>
    <?php include 'loading.php';?>
    <div id="body" style="display:none;">
        <?php include 'layout/header.php';?>
        <main id="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="container d-flex">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="home.php"><i class="bx bx-home"></i> <small> Home</small> </a></li>
                            <li class="page-item active"><a class="page-link" href="#"><i class="bx bx-calendar"></i><small> Transaction</small></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="d-flex flex-column justify-content-center align-items-center">
                <h2 class="page-title"><i class="bx bx-calculator"></i> Transaction </h2>
                <div class="container mt-5 px-2">
                    <div class="table-responsive">
                        <table id="transaction" class="table table-striped table-bordered">
                            <thead class="table-dark">
                                <tr>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">No</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Customer Name</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Staff Name</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Service Type</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Price</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Total Price</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Date</th>
                                    <th scope="col" style="text-align:center;vertical-align: middle;">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="left open-calculator" style="margin-top:30px">
                    <button class="fancy">
                        <span class="top-key"></span>
                        <a id="btn-toggle" href="javascript:void(0)">Open Calculator</a>
                        <span class="bottom-key-1"></span>
                        <span class="bottom-key-2"></span>
                    </button>
                </div>
                <div id="calculator-tool" align="center" style="margin-top:10px;display:none">
                    <?php include 'calculator.php';?>
                </div>

                <!-- Transaction -->
                <div id="EditTransaction" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form enctype='multipart/form-data' method="POST" action="" class="well form-horizontal">
                                <div class="modal-header">
                                    <h5 class="modal-title">Edit Transaction</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i style="color:white" class="fas fa-times-circle"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="card">
                                        <div class="card-body">
                                            <input type="hidden" name="id" id="id">
                                            <div class="form-group row">
                                                <label for="customer_name" class="col-sm-3 col-form-label">Customer Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" autocomplete="off" name="customer_name" id="customer_name" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="staff_name" class="col-sm-3 col-form-label">Staff Name</label>
                                                <div class="col-sm-9">
                                                    <select class="form-select" id="staff_name" name="staff_name">
                                                        <option>Select staff name</option>
                                                        <?php
                                                            $sql = "SELECT * FROM user WHERE user_type = 'staff'";
                                                            $statement = $conn->query($sql);
                                                            $result = $statement->fetch_all(MYSQLI_ASSOC);

                                                            foreach($result as $row){
                                                                echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="service_type" class="col-sm-3 col-form-label">Service Package</label>
                                                <div class="col-sm-9">
                                                    <div class="selectBox" onclick="showCheckboxes()">
                                                        <select class="form-select">
                                                            <option>Select service package</option>
                                                        </select>
                                                        <div class="overSelectEdit"></div>
                                                    </div>
                                                    <div id="checkboxes" style="display:block">
                                                        <?php
                                                        $sql = "SELECT * FROM service";
                                                        $statement = $conn->query($sql);
                                                        $result = $statement->fetch_all(MYSQLI_ASSOC);

                                                        foreach($result as $row){
                                                            $id_key = str_replace(" ","",$row["service_type"]);
                                                            echo '<label><input type="checkbox" class="calcPrice" name="service_type[]" id="'.$id_key.'" value="'.$row["service_type"].'"/> '.$row["service_type"].' - '.$row["title"].'</label>';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="price" class="col-sm-3 col-form-label">Price</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" autocomplete="off" name="price" id="price" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="total_price" class="col-sm-3 col-form-label">Total Price</label>
                                                <div class="col-sm-9">
                                                    <!-- <input type="text" class="form-control" autocomplete="off" name="total_price" id="total_price" readonly> -->
                                                    <div class="input-group mb-3">
                                                        <span class="input-group-text">RM</span>
                                                        <input type="text" class="form-control" autocomplete="off" name="total_price" id="total_price" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="paid" class="col-sm-3 col-form-label">Paid</label>
                                                <div class="col-sm-9">
                                                    <!-- <input type="text" class="form-control" autocomplete="off" name="paid" id="paid" value="RM "> -->
                                                    <div class="input-group mb-3">
                                                        <span class="input-group-text">RM</span>
                                                        <input type="text" class="form-control" autocomplete="off" name="paid" id="paid" onkeypress="return isNumberKey(this);">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row"><div class="col-sm-12"><br></div></div>

                                            <div class="form-group row">
                                                <label for="balance" class="col-sm-3 col-form-label">Balance</label>
                                                <div class="col-sm-9">
                                                    <!-- <input type="text" class="form-control" autocomplete="off" name="balance" id="balance" readonly> -->
                                                    <div class="input-group mb-3">
                                                        <span class="input-group-text">RM</span>
                                                        <input type="text" class="form-control" autocomplete="off" name="balance" id="balance" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <div class="button-row" style="margin-right: 250px;">
                                            <div class="modal-button-save"><a id="btnEdit" title="Edit Transaction"></a></div>
                                        </div>
                                        <div class="button-row" style="margin-right: 250px;">
                                            <div class="modal-button-submit"><a id="btnAdd" title="Submit Transaction"></a></div>
                                        </div>
                                    </center>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Transaction -->
            </section>
        </main>
        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <?php include 'layout/footer.php';?>
    </div>
    <script>
        $('#btnEdit').click(function(){
            var list = $("#EditTransaction input[name='service_type[]']").map(function(){
                if($(this).prop("checked") == true){
                    return $(this).val();
                }
            }).get();
            var service_type = list.join();
            var total_price = $('#EditTransaction #total_price').val();
            total_price = total_price.replace( /^\D+/g,'');
            var paid = $('#EditTransaction #paid').val();
            paid = paid.replace( /^\D+/g,'');
            var balance = $('#EditTransaction #balance').val();
            balance = balance.replace( /^\D+/g,'');
            obj = {
                id:$('#EditTransaction #id').val(),
                staff_name:$('#EditTransaction #staff_name').val(),
                service_type:service_type,
                price:$('#EditTransaction #price').val(),
                total_price:total_price,
                paid:paid,
                balance:balance
            };
            
            $.ajax({
                type:'POST',
                url:'actiontransaction.php?action=edit',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'transaction.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        $('#btnAdd').click(function(){
            var list = $("#EditTransaction input[name='service_type[]']").map(function(){
                if($(this).prop("checked") == true){
                    return $(this).val();
                }
            }).get();
            var service_type = list.join();
            var total_price = $('#EditTransaction #total_price').val();
            total_price = total_price.replace( /^\D+/g,'');
            var paid = $('#EditTransaction #paid').val();
            paid = paid.replace( /^\D+/g,'');
            var balance = $('#EditTransaction #balance').val();
            balance = balance.replace( /^\D+/g,'');
            obj = {
                id:$('#EditTransaction #id').val(),
                staff_name:$('#EditTransaction #staff_name').val(),
                service_type:service_type,
                price:$('#EditTransaction #price').val(),
                total_price:total_price,
                paid:paid,
                balance:balance
            };
            
            $.ajax({
                type:'POST',
                url:'actiontransaction.php?action=submit',
                data:obj,
                success:function(data){
                    if(data.status == true){
                        swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            customClass: 'swal-wide'
                        }).then(()=>{
                            location.href = 'transaction.php'
                        });
                    }else{
                        swal.fire({
                            title: 'Failed!',
                            text: data.message,
                            icon: 'error',
                            customClass: 'swal-wide'
                        });
                    }
                },
            });
        });

        $(document).on('click','.openTransaction',function(){
            var id = $(this).data('id');

            $.ajax({
                url: 'actiontransaction.php?id='+id+'&action=getTransaction',
                success: function(data){
                    var service_type = data.service_type.split(',');
                    $('#EditTransaction #id').val(data.id);
                    $('#EditTransaction #customer_name').val(data.customer_name);
                    $('#EditTransaction #staff_name').val(data.staff_name);
                    $('#EditTransaction #price').val(data.price);
                    for(var i = 0; i<service_type.length; i++){
                        var id_key = service_type[i].replace(/ /g,"");
                        $('#EditTransaction #'+id_key+'').prop('checked',true);  
                    }
                    $('#EditTransaction #total_price').val(parseFloat(data.total_price).toFixed(2));
                    if(data.paid == null){
                        $('#EditTransaction #paid').val('');
                    }else{
                        $('#EditTransaction #paid').val(parseFloat(data.paid).toFixed(2));
                    }
                    if(data.balance == null){
                        $('#EditTransaction #balance').val('');
                    }else{
                        $('#EditTransaction #balance').val(parseFloat(data.balance).toFixed(2));
                    }
                }
            });
        });

        $('.open-calculator').click(function(){
            $('#calculator-tool').toggle();
            $('#btn-toggle').toggle();
            $( "#btn-toggle" ).toggle(function(e) {
                if($(this)[0].innerText == 'CLOSE CALCULATOR'){
                    $('#btn-toggle').text('Open Calculator');
                }else{
                    $('#btn-toggle').text('Close Calculator');
                }
                
            });
        });

        $(document).change(".calcPrice",calcPrice);

        $('.close').click(function(){
            $('.modal').modal('hide');
        });

        $('#paid').keyup(function(){
            var paid = $(this).val();
            if(paid != ''){
                paid = paid.replace( /^\D+/g,'');
                var total_price = $('#total_price').val();
                total_price = total_price.replace( /^\D+/g,'');

                var balance = parseFloat(paid) - parseFloat(total_price);
                $('#balance').val(parseFloat(balance).toFixed(2));
            }else{
                $('#balance').val('');
            }
        });

        function calcPrice() {
            var service_type = [];
            $("input[name^=service_type]:checked").each(function () {
                service_type.push(this.value);
            });

            if(service_type.length != 0){
                $.ajax({
                    url: 'actiontransaction.php?action=getPrice',
                    data:{service_type:service_type},
                    success: function(data){
                        $('#EditTransaction #price').val(data.price);
                        $('#EditTransaction #total_price').val(parseFloat(data.total_price).toFixed(2));
                    }
                });
            }else{
                $('#EditTransaction #price').val('RM 0.00');
                $('#EditTransaction #total_price').val(parseFloat(0).toFixed(2));
            }            
        }

        var expandedEdit = true;
        function showCheckboxes() {
            var checkboxes = document.getElementById("checkboxes");
            if (!expandedEdit) {
                checkboxes.style.display = "block";
                expandedEdit = true;
            } else {
                checkboxes.style.display = "none";
                expandedEdit = false;
            }
        }

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode
            console.log(charCode);
            if ((charCode > 47 && charCode < 58) || charCode == 46) {
                return true;
            }
            return false;
        }
    </script>
</body>
</html>